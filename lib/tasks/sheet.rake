namespace :sheet do
  desc "generate all sheet pattern image"
  task :generate => :environment do
    Sheet.generate_image
  end
end
