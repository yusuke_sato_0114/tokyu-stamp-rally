# coding: utf-8
Time::DATE_FORMATS[:default] = '%Y/%m/%d %H:%M'
Time::DATE_FORMATS[:date] = '%Y/%m/%d'
Time::DATE_FORMATS[:time] = '%H:%M:%S'
Time::DATE_FORMATS[:completed_at] = '%Y年%m月%d日'
Time::DATE_FORMATS[:season_ranking] = '%-m/%-d'
Date::DATE_FORMATS[:default] = '%Y/%m/%d'
