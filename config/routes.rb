# coding: utf-8
Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root  'home#index'

  get   'home/index'
  get   'sheet'                => 'home#sheet',       :as => :sheet
  get   'ranking'              => 'home#ranking',     :as => :ranking
  get   'closed'               => 'home#closed',      :as => :closed

  get   'lottery'              => 'lottery#top',      :as => :lottery
  get   'lottery/ready/:lp_id' => 'lottery#ready',    :as => :ready_lottery
  post  'lottery/cast'         => 'lottery#cast',     :as => :cast_lottery
  get   'lottery/win'          => 'lottery#win',      :as => :win_lottery
  get   'lottery/lose'         => 'lottery#lose',     :as => :lose_lottery
  get   'lottery/not_yet'      => 'lottery#not_yet',  :as => :not_yet_lottery

  get   'enquete'              => 'enquete#new',      :as => :new_enquete
  patch 'enquete/:id'          => 'enquete#answer',   :as => :answer_enquete
  get   'enquete/thanks'       => 'enquete#thanks',   :as => :thanks_enquete

  get   'condition/:token'     => 'condition#show',   :as => :condition
  get   'coupon/:token'        => 'coupon#show',      :as => :coupon
  get   'coupon/:token/used'   => 'coupon#used',      :as => :coupon_used

  get   'contact'              => 'contact#new',      :as => :new_contact
  post  'contact/confirm'      => 'contact#confirm',  :as => :confirm_contact
  post  'contact/complete'     => 'contact#complete', :as => :complete_contact

  devise_for :users, controllers: {
               confirmations:      'users/confirmations',
               passwords:          'users/passwords',
               registrations:      'users/registrations',
               sessions:           'users/sessions',
               unlocks:            'users/unlocks',
               omniauth_callbacks: 'users/omniauth_callbacks'
             }


  devise_scope :user do
    get 'users/password/instructions' => 'users/passwords#instructions',      :as => :instructions_user_password
    get 'users/password/reset'        => 'users/passwords#reset',             :as => :reset_user_password
    get 'users/after_sign_up'         => 'users/registrations#after_sign_up', :as => :after_sign_up_registration
  end

  namespace :api do
    get  'ping'                  => 'ping#index'
    post 'sheet/check_condition'
    post 'sheet/update_name'
    post 'sheet/share_twitter'
    post 'stamp/push'
    post 'coupon/use'
    post 'lottery/share_twitter'
  end

  get '*anywhere'                => 'errors#routing_error'
end
