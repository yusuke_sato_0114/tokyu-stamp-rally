set :output, 'log/crontab.log'

every 1.day, at: '3:30 am' do
  runner "Session.sweep"
  runner "TopRanking.update"
  runner "SelfRanking.update"
end
