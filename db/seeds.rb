# coding: utf-8

# if Rails.env.development? or Rails.env.local?
#   load File.join(Rails.root, 'db', 'seeds_phase_1.rb')
# end

puts '##### CREATE LOTTERY PRIZE #####'
lottery_prizes = CSV.read("#{Rails.root}/db/csv/lottery_prize.csv", headers: true)
lottery_prizes.each do |lp|
  LotteryPrize.create!(
    :id          => lp["id"],
    :stage       => lp["stage"],
    :order_index => lp["order_index"],
    :grade       => lp["grade"],
    :name        => lp["name"].gsub(/\\n/, "\n"),
    :name_long   => lp["name_long"]
  )
end

puts '##### CREATE LOTTERY UNIQUE SERIAL #####'
lottery_unique_serials = CSV.read("#{Rails.root}/db/csv/lottery_code.csv", headers: true)
lottery_unique_serials.each do |lus|
  LotteryUniqueSerial.create!(
    :lottery_prize_id => lus["lottery_prize_id"],
    :lottery_id       => lus["lottery_id"],
    :lottery_pass     => lus["lottery_pass"],
    :lottery_code     => lus["lottery_code"]
  )
end
