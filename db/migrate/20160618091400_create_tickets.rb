class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.references :user, null: false

      t.timestamps
    end
  end
end
