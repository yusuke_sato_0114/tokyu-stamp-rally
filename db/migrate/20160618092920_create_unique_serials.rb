class CreateUniqueSerials < ActiveRecord::Migration
  def change
    create_table :unique_serials do |t|
      t.string     :code,            null: false
      t.boolean    :is_already_used, null: false, default: false

      t.timestamps
    end
  end
end
