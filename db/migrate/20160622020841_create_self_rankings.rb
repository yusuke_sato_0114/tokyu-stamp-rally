class CreateSelfRankings < ActiveRecord::Migration
  def change
    create_table :self_rankings do |t|
      t.integer :type,       null: false
      t.integer :owner,      null: false
      t.integer :owner_rank, null: false
      t.integer :owner_stamp_count
      t.integer :next_1
      t.integer :next_2
      t.integer :next_3
      t.integer :next_4
      t.integer :next_5
      t.integer :prev_1
      t.integer :prev_2
      t.integer :prev_3
      t.integer :prev_4
      t.integer :prev_5
      t.integer :next_1_stamp_count
      t.integer :next_2_stamp_count
      t.integer :next_3_stamp_count
      t.integer :next_4_stamp_count
      t.integer :next_5_stamp_count
      t.integer :prev_1_stamp_count
      t.integer :prev_2_stamp_count
      t.integer :prev_3_stamp_count
      t.integer :prev_4_stamp_count
      t.integer :prev_5_stamp_count
      t.integer :next_1_rank
      t.integer :next_2_rank
      t.integer :next_3_rank
      t.integer :next_4_rank
      t.integer :next_5_rank
      t.integer :prev_1_rank
      t.integer :prev_2_rank
      t.integer :prev_3_rank
      t.integer :prev_4_rank
      t.integer :prev_5_rank
      t.integer :index,   null: false

      t.timestamps
    end
  end
end
