class CreateLotteryPrizes < ActiveRecord::Migration
  def change
    create_table :lottery_prizes do |t|
      t.string  :name,         null: false
      t.string  :name_long,    null: false
      t.integer :stage,        null: false, default: 1
      t.integer :order_index,  null: false, default: 10
      t.string  :grade
      t.boolean :is_available, null: false, default: true

      t.timestamps
    end
  end
end
