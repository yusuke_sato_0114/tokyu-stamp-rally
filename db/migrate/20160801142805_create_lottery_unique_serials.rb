class CreateLotteryUniqueSerials < ActiveRecord::Migration
  def change
    create_table :lottery_unique_serials do |t|
      t.references :lottery_prize,   null: false
      t.references :user
      t.integer    :number,          null: false
      t.string     :lottery_id,      null: false
      t.string     :lottery_pass,    null: false
      t.string     :lottery_code,    null: false
      t.boolean    :is_already_used, null: false, default: false

      t.timestamps
    end
  end
end
