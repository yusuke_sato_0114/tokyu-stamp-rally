class CreateStamps < ActiveRecord::Migration
  def change
    create_table :stamps do |t|
      t.references :check_point
      t.references :coupon
      t.string     :name,           null: false
      t.string     :device_id,      null: false
      t.integer    :type,           null: false, default: 20
      t.boolean    :is_available,   null: false, default: false
      t.string     :image_filename, default: ''
      t.integer    :use_count,      null: false, default: 0
      t.integer    :number

      t.timestamps
    end
  end
end
