class CreateTopRankings < ActiveRecord::Migration
  def change
    create_table :top_rankings do |t|
      t.integer :type,        null: false
      t.integer :user_id,     null: false
      t.integer :rank,        null: false
      t.integer :stamp_count, null: false
      t.integer :index,       null: false

      t.timestamps
    end
  end
end
