class CreateConditions < ActiveRecord::Migration
  def change
    create_table :conditions do |t|
      t.references :coupon
      t.integer    :phase,       null: false
      t.integer    :sheet_count, null: true
      t.integer    :stamp_count, null: false
      t.string     :name,        null: false
      t.text       :description
      t.string     :image_filename
      t.string     :audio_filename
      t.integer    :priority,    null: false, default: 20
      t.string     :token

      t.timestamps
    end
  end
end
