class CreateSheets < ActiveRecord::Migration
  def change
    create_table :sheets do |t|
      t.references :user,            null: false
      t.string     :name,            null: true
      t.integer    :number,          null: false
      t.integer    :phase,           null: false, default: 1
      t.string     :token,           null: false
      t.boolean    :is_current,      null: false, default: true
      t.boolean    :is_complete,     null: false, default: false
      t.datetime   :completed_at

      t.timestamps
    end

    add_index :sheets, :token, :unique => true
  end
end
