class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.integer :phase,                     null: false
      t.string  :name,                      null: false
      t.string  :description,               null: false
      t.integer :type,                      null: false, default: 10
      t.integer :get_count,                 null: false, default: 1
      t.string  :token,                     null: false
      t.integer :index_order,               null: false
      t.string  :list_image_filename,       null: false
      t.string  :list_blank_image_filename, null: false
      t.string  :detail_image_filename,     null: false

      t.timestamps
    end
  end
end
