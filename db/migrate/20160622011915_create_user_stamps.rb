class CreateUserStamps < ActiveRecord::Migration
  def change
    create_table :user_stamps do |t|
      t.references :user,  null: false
      t.references :stamp, null: false

      t.timestamps
    end
  end
end
