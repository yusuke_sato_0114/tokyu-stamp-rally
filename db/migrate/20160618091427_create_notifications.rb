class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string   :title,        null: false
      t.text     :content,      null: false
      t.boolean  :is_published, null: false, default: false
      t.datetime :published_at, null: false

      t.timestamps
    end
  end
end
