class CreateConditionCheckPoints < ActiveRecord::Migration
  def change
    create_table :condition_check_points do |t|
      t.references :condition,      null: false
      t.references :check_point,    null: false
      t.boolean    :is_unspecified, null: false, default: false
      t.integer    :order,          null: true

      t.timestamps
    end
  end
end
