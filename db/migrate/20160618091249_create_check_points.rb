class CreateCheckPoints < ActiveRecord::Migration
  def change
    create_table :check_points do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
