class CreateUserCoupons < ActiveRecord::Migration
  def change
    create_table :user_coupons do |t|
      t.references :user,   null: false
      t.references :coupon, null: false
      t.integer    :number, null: false, default: 0

      t.timestamps
    end
  end
end
