class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string   :name
      t.integer  :theme
      t.string   :token,                      null: false
      t.string   :uid
      t.string   :twitter_screen_name
      t.string   :provider,                   null: false
      t.boolean  :answered_enquete,           null: false, default: false
      t.integer  :birthyear
      t.integer  :gender
      t.integer  :frequency_of_use_tokyu
      t.integer  :job
      t.string   :serial_code
      t.string   :zip_code
      t.boolean  :is_authenticated_by_serial, null: false, default: false
      t.boolean  :is_available,               null: false, default: true
      t.datetime :last_accessed_at
      t.string   :twitter_access_token
      t.string   :twitter_secret_token

      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email

      t.timestamps null: false
    end

    # add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    add_index :users, :confirmation_token,   unique: true
    add_index :users, :token, :unique => true
  end
end
