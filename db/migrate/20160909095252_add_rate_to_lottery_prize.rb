class AddRateToLotteryPrize < ActiveRecord::Migration
  def change
    add_column :lottery_prizes, :rate, :float, :null => false, :default => 0.01
  end
end
