class CreateSheetStamps < ActiveRecord::Migration
  def change
    create_table :sheet_stamps do |t|
      t.references :sheet,      null: false
      t.references :stamp,      null: false
      t.integer    :position,   null: false

      t.timestamps
    end
  end
end
