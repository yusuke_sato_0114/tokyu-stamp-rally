# coding: utf-8

puts '##### CREATE ADMIN USER #####'
AdminUser.create!(email: 'admin@tokyu.rev-stamp.com', password: '99f3b8cf27441', password_confirmation: '99f3b8cf27441')

puts '##### CREATE CHECK POINT #####'
check_points = CSV.read("#{Rails.root}/db/csv/check_point.csv", headers: true)
check_points.each do |check_point|
  CheckPoint.create!(
    :id   => check_point["id"],
    :name => check_point["name"]
  )
  puts "create check point #{check_point["name"]}"
end

puts '##### CREATE STAMP #####'
stamps = CSV.read("#{Rails.root}/db/csv/stamp.csv", headers: true)
stamps.each do |stamp|
  Stamp.create!(
    :check_point_id => stamp["check_point_id"],
    :coupon_id      => stamp["coupon_id"].presence || nil,
    :number         => stamp["number"].present? ? stamp["number"].to_i : nil,
    :device_id      => stamp["device_id"],
    :type           => stamp["type"],
    :image_filename => stamp["image_filename"].presence || nil,
    :is_available   => stamp["is_available"],
    :name           => stamp["name"]
  )
  puts "create stamp #{stamp["name"]}"
end

puts '##### CREATE COUPON #####'
coupons = CSV.read("#{Rails.root}/db/csv/coupon.csv", headers: true)
coupons.each do |coupon|
  Coupon.create!(
    :id                        => coupon["id"],
    :phase                     => coupon["phase"].to_i,
    :index_order               => coupon["index_order"].to_i,
    :name                      => coupon["name"],
    :description               => coupon["description"].gsub(/\\n/, "\n"),
    :get_count                 => coupon["get_count"] || 1,
    :type                      => coupon["type"].present? ? coupon["type"].to_i : 10,
    :list_image_filename       => coupon["list_image_filename"],
    :list_blank_image_filename => coupon["list_blank_image_filename"],
    :detail_image_filename     => coupon["detail_image_filename"]
  )
  puts "create coupon #{coupon["name"]}"
end

puts '##### CREATE CONDITION #####'
conditions = CSV.read("#{Rails.root}/db/csv/condition.csv", headers: true)
conditions.each do |condition|
  Condition.create!(
    :id             => condition["id"],
    :phase          => condition["phase"].present? ? condition["phase"].to_i : nil,
    :coupon_id      => condition["coupon_id"],
    :sheet_count    => condition["sheet_count"].present? ? condition["sheet_count"].to_i : nil,
    :stamp_count    => condition["stamp_count"],
    :name           => condition["name"],
    :description    => condition["description"].gsub(/\\n/, "\n"),
    :priority       => condition["priority"].present? ? condition["priority"].to_i : 20,
    :image_filename => condition["image_filename"].presence,
    :audio_filename => condition["audio_filename"].presence
  )
  puts "create condition #{condition["name"]}"
end

puts '##### CREATE CONDITION CHECK POINT #####'
condition_check_points = CSV.read("#{Rails.root}/db/csv/condition_check_point.csv", headers: true)
condition_check_points.each do |condition_check_point|
  ConditionCheckPoint.create!(
    :condition_id   => condition_check_point["condition_id"],
    :check_point_id => condition_check_point["check_point_id"],
    :is_unspecified => condition_check_point["is_unspecified"] || false,
    :order          => condition_check_point["order"].present? ? condition_check_point["order"].to_i : nil
  )
end


if Rails.env.local? or Rails.env.development?
  puts '##### CREATE DUMMY USER #####'
  1.upto 20 do |i|
    u = User.new(
      :name                       => Faker::Japanese::Name.name,
      :theme                      => User.themes.keys.sample,
      :provider                   => 'twitter',
      :answered_enquete           => true,
      :is_authenticated_by_serial => true
    )
    u.skip_confirmation!
    u.save!
    puts "create dummy user #{i}"
  end

  puts '##### CREATE DUMMY USER STAMPS #####'
  1.upto 20 do |i|
    u = User.find(i)
    (0..20).to_a.sample.times do
      u.add_stamp Stamp.add.sample
      u.user_stamps.last.update_attributes! :created_at => Time.mktime(Time.now.year, Time.now.month, (1.week.ago.day..Time.now.day).to_a.sample)
    end
    puts "create dummy user stamps user #{i}"
  end
end

puts '##### CREATE UNIQUE SERIALS #####'
if Rails.env.local? or Rails.env.development?
  codes = CSV.read("#{Rails.root}/db/csv/serial_codes.csv").slice(99000,99999)
else
  codes = CSV.read("#{Rails.root}/db/csv/serial_codes.csv")
end

codes.each.with_index(1) do |code, i|
  UniqueSerial.create!(
    :code => code[0]
  )
  puts "create unique serials #{i}" if i % 1000 == 0
end

puts '##### CREATE NOTIFICATION #####'
notifications = CSV.read("#{Rails.root}/db/csv/notification.csv", headers: true)
notifications.each do |notification|
  Notification.create!(
    :title        => notification["title"],
    :content      => notification["content"],
    :is_published => notification["is_published"],
    :published_at => Time.parse(notification["published_at"])
  )
  puts "create notification #{notification["title"]}"
end
