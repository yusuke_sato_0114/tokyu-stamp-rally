# coding: utf-8
ActiveAdmin.register LotteryPrize do
  menu priority: 9

  actions :index, :new, :create, :edit, :update, :destroy

  config.per_page = 100

  permit_params :name, :name_long, :stage, :rate, :order_index

  filter :name
  filter :name_long
  filter :stage
  filter :is_available

  batch_action :unavailable, confirm: "選択した抽選賞品を利用停止していいですか？" do |ids|
    LotteryPrize.find(ids).each do |lottery_prize|
      lottery_prize.update_attributes! :is_available => false
    end
    redirect_to collection_path, alert: "利用停止しました"
  end

  batch_action :available, confirm: "選択した抽選賞品の利用停止を解除してもいいですか？" do |ids|
    LotteryPrize.find(ids).each do |lottery_prize|
      lottery_prize.update_attributes! :is_available => true
    end
    redirect_to collection_path, alert: "利用停止を解除しました"
  end

  index do
    selectable_column
    column :name
    column :name_long
    column :stage
    column :order_index
    column :grade
    column :rate do |lottery_prize|
      "#{lottery_prize.rate * 100}%"
    end
    column :is_available
    column :all_count do |lottery_prize|
      lottery_prize.lottery_unique_serials.count
    end
    column :rest_count do |lottery_prize|
      lottery_prize.lottery_unique_serials.not_used.count
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :name_long
      f.input :stage
      f.input :grade
      f.input :rate
      f.input :order_index
    end
    actions
  end
end
