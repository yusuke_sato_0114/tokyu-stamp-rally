# coding: utf-8
ActiveAdmin.register UniqueSerial do
  menu priority: 6
  batch_action :destroy, false
  actions :index

  config.per_page = 200

  filter :code
  filter :is_already_used

  batch_action :used, confirm: "選択したシリアルコードを使用済みにしていいですか？" do |ids|
    UniqueSerial.find(ids).each do |unique_serial|
      unique_serial.update_attributes! :is_already_used => true
    end
    redirect_to collection_path, alert: "使用済みにしました"
  end

  batch_action :unused, confirm: "選択したシリアルコードを未使用にしていいですか？" do |ids|
    UniqueSerial.find(ids).each do |unique_serial|
      unique_serial.update_attributes! :is_already_used => false
    end
    redirect_to collection_path, alert: "未使用にしました"
  end

  index do
    selectable_column
    column :code
    column :is_already_used do |unique_serial|
      status_tag unique_serial.is_already_used
    end
  end
end
