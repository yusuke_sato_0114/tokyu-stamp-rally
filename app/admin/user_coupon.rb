ActiveAdmin.register UserCoupon do
  menu false

  actions :index

  config.per_page = 100

  filter :coupon_id, as: :select, collection: Coupon.all.map { |coupon| [coupon.name,coupon.id] }
  filter :created_at
  filter :updated_at
  filter :number

  index do
    column :coupon_id do |user_coupon|
      user_coupon.coupon.name
    end
    column :number
    column :created_at do |user_coupon|
      user_coupon.created_at.to_s
    end
    column :updated_at do |user_coupon|
      user_coupon.updated_at.to_s
    end
  end
end
