ActiveAdmin.register TopRanking do
  menu priority: 8

  actions :index
  filter :type, as: :select, collection: TopRanking.types.map { |type, value| [I18n.t("activerecord.enum.top_ranking.type.#{type}"), value] }

  config.sort_order = 'rank_asc'

  index do
    column :type do |top_ranking|
      t("activerecord.enum.top_ranking.type.#{top_ranking.type}")
    end
    column :rank
    column :user_id do |top_ranking|
      link_to top_ranking.user_id, admin_user_path(top_ranking.user_id)
    end
    column :stamp_count
  end
end
