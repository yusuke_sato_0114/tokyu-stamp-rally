# coding: utf-8
ActiveAdmin.register Stamp do
  menu priority: 3

  actions :index, :new, :create, :edit, :update, :destroy

  permit_params :name, :device_id, :type, :check_point_id, :coupon_id, :image_filename
  
  filter :check_point
  filter :is_available

  batch_action :unavailable, confirm: "選択したスタンプを利用停止していいですか？" do |ids|
    Stamp.find(ids).each do |stamp|
      stamp.update_attributes! :is_available => false
    end
    redirect_to collection_path, alert: "利用停止しました"
  end

  batch_action :available, confirm: "選択したスタンプの利用停止を解除してもいいですか？" do |ids|
    Stamp.find(ids).each do |stamp|
      stamp.update_attributes! :is_available => true
    end
    redirect_to collection_path, alert: "利用停止を解除しました"
  end

  index do
    selectable_column
    column :is_available
    column :name
    column :device_id
    column :type do |stamp|
      t("activerecord.enum.stamp.type.#{stamp.type}")
    end
    column :image_filename
    column :image do |stamp|
      if stamp.image_filename
        image_tag stamp.image_path, :width => 100
      else
        '画像なし'
      end
    end
    column :check_point do |stamp|
      link_to stamp.check_point.name, admin_check_point_path(stamp.check_point) if stamp.check_point_id.present?
    end
    column :coupon do |stamp|
      if stamp.coupon
        link_to stamp.coupon.name, admin_coupon_path(stamp.coupon)
      else
        'なし'
      end
    end
    column :use_count
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :device_id
      f.input :type, as: :select, collection: Stamp.types.keys.map { |type| [t("activerecord.enum.stamp.type.#{type}"), type] }, include_blank: false
      f.input :check_point
      f.input :image_filename
      f.input :coupon
    end
    actions
  end
end
