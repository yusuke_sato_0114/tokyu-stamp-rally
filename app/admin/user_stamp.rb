ActiveAdmin.register UserStamp do
  menu false

  actions :index

  config.per_page = 100

  filter :stamp_id, as: :select, collection: CheckPoint.all.map { |check_point| [check_point.name,check_point.stamps.pluck(:id).join(',')] }
  filter :created_at

  index do
    column :stamp_id do |user_stamp|
      user_stamp.stamp.check_point.name
    end
    column :created_at do |user_stamp|
      user_stamp.created_at.to_s
    end
  end
end
