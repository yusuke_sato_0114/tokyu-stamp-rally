# coding: utf-8
ActiveAdmin.register User do
  menu priority: 1

  actions :index, :show

  config.per_page = 100

  filter :name
  filter :theme, as: :select, collection: User.themes.map { |theme, value| [I18n.t("activerecord.enum.user.theme.#{theme}"), value] }
  filter :provider, as: :select, collection: [['Twitter','twitter'],['Email','email']]
  filter :birthyear
  filter :gender, as: :select, collection: User.genders.map { |gender, value| [I18n.t("activerecord.enum.user.gender.#{gender}"), value] }
  filter :frequency_of_use_tokyu, as: :select, collection: User.frequency_of_use_tokyus.map { |frequency_of_use_tokyu, value| [I18n.t("activerecord.enum.user.frequency_of_use_tokyu.#{frequency_of_use_tokyu}"), value] }
  filter :job, as: :select, collection: User.jobs.map { |job, value| [I18n.t("activerecord.enum.user.job.#{job}"), value] }
  filter :last_accessed_at
  filter :created_at
  filter :serial_code

  action_item :add_user_stamp, only: :show do
    link_to 'このユーザに手動でスタンプ付与', admin_add_user_stamp_path(:user_stamp => { :user_id => params[:id] })
  end

  action_item :add_user_coupon, only: :index do
    if Rails.env.local? or Rails.env.development?
      link_to 'プレゼント抽選クーポン付加', add_user_coupon_admin_users_path, 'data-confirm': '全ユーザにプレゼント抽選クーポンを5枚ずつ付与していいですか？', method: :post
    end
  end

  action_item :update_ranking, only: :index do
    link_to 'ランキング更新', update_stamp_ranking_admin_users_path, 'data-confirm': 'ランキングを更新していいですか？', method: :post
  end

  action_item :display_user_coupon, only: :index do
    link_to 'ユーザ所持クーポンを見る', admin_user_coupons_path
  end

  action_item :user_count_transition, only: :index do
    link_to 'ユーザ数推移を見る', admin_user_count_transition_path
  end

  collection_action :update_stamp_ranking, method: :post do
    TopRanking.update
    SelfRanking.update
    redirect_to admin_users_path, notice: 'スタンプランキングを更新しました'
  end

  collection_action :add_user_coupon, method: :post do
    coupon_id = Coupon.present_ticket.first.id

    User.all.each do |user|
      if user.user_coupons.where(:coupon_id => coupon_id).first.present?
        UserCoupon.where(:user_id => user.id, :coupon_id => coupon_id).first.increment! :number, 5
      else
        user.user_coupons.create!(
          :coupon_id => coupon_id,
          :number    => 5
        )
      end
    end

    redirect_to collection_path, notice: '全ユーザーに抽選クーポンを5枚付与しました'
  end

  batch_action :unavailable, confirm: "選択したユーザを利用停止していいですか？" do |ids|
    User.find(ids).each do |user|
      user.update_attributes! :is_available => false
    end
    redirect_to collection_path, alert: "利用停止しました"
  end

  batch_action :available, confirm: "選択したユーザの利用停止を解除してもいいですか？" do |ids|
    User.find(ids).each do |user|
      user.update_attributes! :is_available => true
    end
    redirect_to collection_path, alert: "利用停止を解除しました"
  end

  index do
    selectable_column
    column :name
    column :email_or_screen_name do |user|
      user.email.presence || user.twitter_screen_name
    end
    column :theme do |user|
      t("activerecord.enum.user.theme.#{user.theme}")
    end
    column :birthyear
    column :gender do |user|
      t("activerecord.enum.user.gender.#{user.gender}")
    end
    column :frequency_of_use_tokyu do |user|
      t("activerecord.enum.user.frequency_of_use_tokyu.#{user.frequency_of_use_tokyu}")
    end
    column :job do |user|
      t("activerecord.enum.user.job.#{user.job}")
    end
    column :is_available do |user|
      status_tag user.is_available
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row :provider
      row :email_or_screen_name do |user|
        user.email.presence || user.twitter_screen_name
      end
      row :theme do |user|
        t("activerecord.enum.user.theme.#{user.theme}")
      end
      row :birthyear
      row :gender do |user|
        t("activerecord.enum.user.gender.#{user.gender}")
      end
      row :zip_code
      row :frequency_of_use_tokyu do |user|
        t("activerecord.enum.user.frequency_of_use_tokyu.#{user.frequency_of_use_tokyu}")
      end
      row :job do |user|
        t("activerecord.enum.user.job.#{user.job}")
      end
      row :serial_code
      row :is_available
      row :token
    end

    panel 'スタンプシート' do
      table_for user.sheets do
        column :name
        column :token
        column :phase
        column :stamps do |sheet|
          table_for sheet.sheet_stamps do
            column :position
            column :name do |sheet_stamp|
              sheet_stamp.stamp.name
            end
          end
        end
      end
    end

    panel 'クーポン' do
      table_for user.user_coupons do
        column :name do |user_coupon|
          user_coupon.coupon.name
        end
        column :number
      end
    end

    panel '獲得賞品' do
      table_for user.lottery_unique_serials do
        column :prize_name do |lottery_unique_serial|
          "#{lottery_unique_serial.lottery_prize.grade}賞 #{lottery_unique_serial.lottery_prize.name}"
        end
        column :stage do |lottery_unique_serial|
          lottery_unique_serial.lottery_prize.stage
        end
        column :lottery_id do |lottery_unique_serial|
          lottery_unique_serial.lottery_id
        end
        column :lottery_pass do |lottery_unique_serial|
          lottery_unique_serial.lottery_pass
        end
        column :lottery_code do |lottery_unique_serial|
          lottery_unique_serial.lottery_code
        end
      end
    end
  end
end
