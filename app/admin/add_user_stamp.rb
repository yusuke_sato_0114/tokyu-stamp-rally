# coding: utf-8
ActiveAdmin.register_page 'add_user_stamp' do
  menu false

  controller do
    before_action :redirect_if_user_id_blank, :only => [ :index ]
    before_action :set_user
    before_action :set_stamp, :only => [ :create ]

    private
    def redirect_if_user_id_blank
      redirect_to admin_users_path, notice: 'パラメータにユーザIDをつけてアクセスしてください' and return if user_stamp_params[:user_id].blank?
    end

    def set_user
      @user = User.where(:id => user_stamp_params[:user_id]).first
      redirect_to admin_users_path, notice: '存在しないユーザです' and return if @user.blank?
    end

    def set_stamp
      @stamp = Stamp.where(:id => user_stamp_params[:stamp_id]).first
      redirect_to admin_users_path, notice: '存在しないスタンプです' and return if @stamp.blank?
    end

    def user_stamp_params
      params.require(:user_stamp).permit(:user_id, :stamp_id)
    end
  end

  page_action :create, :method => 'post' do
    current_sheet = @user.sheets.current
    @user.add_stamp @stamp

    hit_condition = current_sheet.check_condition
    @user.get_coupon hit_condition if hit_condition.present?
    
    redirect_to admin_user_path(@user.id), notice: 'スタンプを付与しました'
  end

  content :title => 'ユーザスタンプ付与画面' do
    panel 'ユーザ情報' do
      attributes_table_for user do
        row :id
        row :name
        row :email_or_screen_name do
          user.email.presence || user.twitter_screen_name
        end
      end
    end
    active_admin_form_for :user_stamp, :url => admin_add_user_stamp_create_path, :method => 'post' do |f|
      panel '追加するスタンプ' do
        f.inputs do
          f.input :user_id, as: :hidden, :input_html => { :value => user.id }
          f.input :stamp_id, label: 'スタンプ', as: :select, include_blank: false, collection: Stamp.add.available.map { |stamp| ["#{stamp.name}(#{stamp.device_id})",stamp.id] }
        end
      end
      f.actions do
        f.submit '追加する'
      end
    end
  end
end
