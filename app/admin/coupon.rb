# coding: utf-8
ActiveAdmin.register Coupon do
  menu priority: 4
  actions :index, :show, :edit, :update, :destroy

  permit_params :name, :description, :type, :get_count
  
  filter :type, as: :select, collection: Coupon.types.map { |type, value| [I18n.t("activerecord.enum.coupon.type.#{type}"), value] }

  action_item :toggle_coupon_phase, only: :index do
    present_ticket_coupon = Coupon.present_ticket.first

    if Settings.phase.to_i === 1 and present_ticket_coupon.phase === 2
      link_to 'クーポンをフェーズ1の状態にする', toggle_phase_admin_coupons_path, 'data-confirm': 'クーポンの状態をフェーズ1の状態に切り替えていいですか？', method: :post
    elsif Settings.phase.to_i === 2 and present_ticket_coupon.phase === 1
      link_to 'クーポンをフェーズ2の状態にする', toggle_phase_admin_coupons_path, 'data-confirm': 'クーポンの状態をフェーズ2の状態に切り替えていいですか？', method: :post
    end
  end

  collection_action :toggle_phase, method: :post do
    present_ticket_coupon = Coupon.present_ticket.first
    normal_coupon = Coupon.normal.current_phase.first
    next_phase = present_ticket_coupon.phase === 1 ? 2 : 1

    present_ticket_coupon.update_attributes! :phase => next_phase
    present_ticket_coupon.conditions.each do |c|
      c.update_attributes! :phase => next_phase
    end

    Stamp.use.available.each do |s|
      normal_coupon.stamps << s
    end

    redirect_to admin_coupons_path, notice: 'クーポンの状態を切り替えました'
  end

  index do
    column :name
    column :description do |coupon|
      hbr raw coupon.description
    end
    column :type do |coupon|
      t("activerecord.enum.coupon.type.#{coupon.type}")
    end
    column :get_count
    column :phase
    actions
  end

  show do
    attributes_table do
      row :name
      row :description do |coupon|
        hbr raw coupon.description
      end
      row :type do |coupon|
        t("activerecord.enum.coupon.type.#{coupon.type}")
      end
      row :get_count
      row :phase
      row :list_image_filename do |coupon|
        image_tag coupon.list_image_path, width: 150
      end
      row :list_blank_image_filename do |coupon|
        image_tag coupon.list_blank_image_path, width: 150
      end
      row :detail_image_filename do |coupon|
        image_tag coupon.detail_image_path, width: 150
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :description, as: :text
      f.input :type, as: :select, include_blank: false, collection: Coupon.types.keys.map { |type| [t("activerecord.enum.coupon.type.#{type}"), type] }
      f.input :get_count
      f.input :phase
    end
    actions
  end
end
