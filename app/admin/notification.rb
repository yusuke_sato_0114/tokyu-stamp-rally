# coding: utf-8
ActiveAdmin.register Notification do
  menu priority: 7

  actions :index, :show, :new, :create, :edit, :update, :destroy
  permit_params :title, :content, :published_at_date, :published_at_time_hour, :published_at_time_minute

  filter :title

  batch_action :publish, confirm: "選択したお知らせの公開フラグをオンにします" do |ids|
    Notification.find(ids).each do |notification|
      notification.update_attributes! :is_published => true
    end
    redirect_to collection_path, alert: "公開フラグをオンにしました"
  end

  batch_action :hidden, confirm: "選択したお知らせの公開フラグをオフにします" do |ids|
    Notification.find(ids).each do |notification|
      notification.update_attributes! :is_published => false
    end
    redirect_to collection_path, alert: "公開フラグをオフにしました"
  end

  index do
    selectable_column
    column :title
    column :is_published do |notification|
      status_tag notification.is_published
    end
    column :published_at do |notification|
      notification.published_at.to_s
    end
    actions
  end

  show do
    attributes_table do
      row :title
      row :content
      row :is_published do |notification|
        status_tag notification.is_published
      end
      row :published_at do |notification|
        notification.published_at.to_s
      end
    end
  end

  form do |f|
    inputs do
      input :title
      input :content
      input :published_at, as: :just_datetime_picker
    end
    actions
  end
end
