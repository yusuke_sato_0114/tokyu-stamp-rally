# coding: utf-8
ActiveAdmin.register LotteryUniqueSerial do
  menu priority: 10
  batch_action :destroy, false
  actions :index, :new, :create, :edit, :update, :destroy

  config.per_page = 100

  permit_params :lottery_prize_id, :lottery_id, :lottery_pass, :lottery_code

  filter :lottery_prize_id, as: :select, collection: LotteryPrize.all.map { |lottery_prize| [lottery_prize.name, lottery_prize.id] }
  filter :lottery_id
  filter :lottery_pass
  filter :lottery_code
  filter :is_already_used

  batch_action :used, confirm: "選択した抽選シリアルコードを使用済みにしていいですか？" do |ids|
    LotteryUniqueSerial.find(ids).each do |lus|
      lus.update_attributes! :is_already_used => true
    end
    redirect_to collection_path, alert: "使用済みにしました"
  end

  batch_action :unused, confirm: "選択した抽選シリアルコードを未使用にしていいですか？" do |ids|
    LotteryUniqueSerial.find(ids).each do |lus|
      lus.update_attributes! :is_already_used => false
    end
    redirect_to collection_path, alert: "未使用にしました"
  end

  collection_action :download_update_log_csv, :method => :get do
    csv = CSV.generate do |csv|
      csv << ['賞品名', '取得日時']

      LotteryPrize.all.each do |lp|
        lp.lottery_unique_serials.order('updated_at asc').each do |lus|
          csv << [lp.name.gsub(/\n/,''), lus.updated_at]
        end
      end
    end

    send_data(
      csv.encode('Shift_JIS', :invalid => :replace, :undef => :replace),
      type: 'text/csv; charset=shift_jis; header=present',
      disposition: "attachment; filename=lottery_unique_serial_update_log.csv"
    )
  end

  action_item(:export_update_log_csv, only: :index) do
    link_to '賞品の当選日時ログをダウンロード', params.merge(:action => :download_update_log_csv)
  end

  index do
    selectable_column
    column :lottery_prize_id do |lus|
      lus.lottery_prize.name
    end
    column :stage do |lus|
      lus.lottery_prize.stage
    end
    column :user do |lus|
      link_to lus.user.name, admin_user_path(lus.user) if lus.user.present?
    end
    column :number
    column :lottery_id
    column :lottery_pass
    column :lottery_code
    column :is_already_used
    actions
  end

  form do |f|
    f.inputs do
      f.input :lottery_prize_id, as: :select, collection: LotteryPrize.all.map { |lottery_prize| [lottery_prize.name,lottery_prize.id] }, include_blank: false
      f.input :lottery_id
      f.input :lottery_pass
      f.input :lottery_code
    end
    actions
  end
end
