# coding: utf-8
ActiveAdmin.register_page 'user_count_transition' do
  menu false

  content :title => 'ユーザ数推移' do
    panel '日付とその時点での総ユーザ数' do
      table_for User.get_count_by_created_at do
        column '日付', :created_at
        column 'ユーザ数', :user_count
      end
    end
  end
end
