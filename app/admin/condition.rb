ActiveAdmin.register Condition do
  menu priority: 5
  actions :index, :show, :edit, :update, :destroy

  permit_params :name, :description, :sheet_count, :stamp_count, :phase, :priority

  filter :name
  filter :phase
  filter :sheet_count
  filter :stamp_count
  filter :priority

  index do
    column :name
    column :description do |condition|
      hbr condition.description
    end
    column :sheet_count
    column :stamp_count
    column :phase
    column :priority
    column :coupon do |condition|
      link_to condition.coupon.name, admin_coupon_path(condition.coupon)
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row :description do |condition|
        hbr condition.description
      end
      row :sheet_count
      row :stamp_count
      row :phase
      row :priority
      row :coupon do |condition|
        link_to condition.coupon.name, admin_coupon_path(condition.coupon)
      end
      row :image_filename do |condition|
        image_tag condition.image_path
      end
      row :audio_filename do |condition|
        audio_tag condition.audio_path, controls: true if condition.audio_filename.present?
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      f.input :sheet_count
      f.input :stamp_count
      f.input :phase
      f.input :priority
    end
    actions
  end
end
