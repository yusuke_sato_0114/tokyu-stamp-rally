# coding: utf-8
ActiveAdmin.register CheckPoint do
  menu priority: 2

  permit_params :name,
                :stamps_attributes => [ :name, :device_id, :type, :_destroy, :id ]

  filter :name

  controller do
    helper :application
  end

  action_item :display_user_stamp, only: :index do
    link_to '各駅のスタンプ状況を見る', admin_user_stamps_path
  end

  index do
    selectable_column
    column :name
    column :stamps do |check_point|
      check_point.stamps.map { |stamp| hbr "#{stamp.name}(#{stamp.device_id})\n" }
    end
    column :stamps_use_count do |check_point|
      check_point.stamps.sum :use_count
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row :stamps_use_count do |check_point|
        check_point.stamps.sum :use_count
      end
    end

    panel 'スタンプ' do
      table_for check_point.stamps do
        column :name
        column :device_id
        column :type do |stamp|
          t("activerecord.enum.stamp.type.#{stamp.type}")
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.has_many :stamps, allow_destroy: true, new_record: true do |c|
        c.input :name
        c.input :device_id
        c.input :type, as: :select, collection: Stamp.types.keys.map { |type| [t("activerecord.enum.stamp.type.#{type}"), type] }, include_blank: false
      end
    end
    actions
  end
end
