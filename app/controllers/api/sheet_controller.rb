# coding: utf-8
class Api::SheetController < Api::ApplicationController
  before_action :set_current_user, :only => [ :check_condition, :share_twitter ]
  before_action :set_sheet_by_token
  before_action :set_twitter_client, :only => [ :share_twitter ]

  def check_condition
    hit_condition = @sheet.check_condition

    if hit_condition.present?
      coupon = @user.get_coupon hit_condition 

      success(
        {
          :condition => hit_condition,
          :condition_path => Rails.application.routes.url_helpers.condition_path(hit_condition.token)
        }
      )
    else
      success( :condition => nil )
    end
  end

  def update_name
    @sheet.update_attributes! :name => params[:sheet][:name]
    success( { :sheet => { :name => @sheet.name } } )
  end

  def share_twitter
    tweet_text = params[:tweet][:content]
    error 422 if tweet_text.blank?

    @twitter_client.update_with_media(
      "#{tweet_text} #{Settings.site.url}",
      open(@sheet.share_image_url)
    )

    success
  end

  private
  def set_sheet_by_token
    @sheet = Sheet.where(:token => params[:sheet][:token]).first
    error_404 if @sheet.blank?
  end
end
