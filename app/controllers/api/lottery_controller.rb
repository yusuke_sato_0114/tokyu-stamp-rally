class Api::LotteryController < Api::ApplicationController
  before_action :set_twitter_client

  def share_twitter
    lottery_prize = LotteryPrize.where(:id => params[:lottery_prize][:id]).first
    error 404 if lottery_prize.blank?

    tweet_text = params[:tweet][:content]
    error 422 if tweet_text.blank?

    @twitter_client.update_with_media(
      "#{tweet_text} #{Settings.site.url}",
      open(lottery_prize.win_image_url)
    )

    success
  end
end
