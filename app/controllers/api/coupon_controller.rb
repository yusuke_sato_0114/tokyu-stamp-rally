class Api::CouponController < Api::ApplicationController
  before_action :login_required
  before_action :set_current_user
  before_action :set_coupon

  def use
    stamp = Stamp.where(:device_id => params[:stamp][:device_id]).first
    coupon = @user_coupon.coupon

    error_404 and return if stamp.blank? or !stamp.is_available or !stamp.use? or coupon.stamps.exclude? stamp or @user_coupon.number <= 0

    # @user_coupon.decrement! :number
    stamp.increment! :use_count, 1

    success(
      {
        :valid => true,
        :coupon => {
          :id          => coupon.id,
          :name        => coupon.name,
          :description => coupon.description,
          :count       => @user_coupon.number,
          :used_path   => Rails.application.routes.url_helpers.coupon_used_path(:token => coupon.token)
        }
      }
    )
  end

  private
  def set_coupon
    @user_coupon = @user.user_coupons.where(:coupon_id => params[:coupon][:id]).first
    error_404 and return if @user_coupon.blank?
  end
end
