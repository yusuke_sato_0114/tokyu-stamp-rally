# coding: utf-8
class Api::StampController < Api::ApplicationController
  before_action :login_required
  before_action :set_current_user

  def push
    valid = true
    sheet = @user.sheets.current
    stamp = Stamp.where(:device_id => params[:stamp][:device_id]).first
    check_point = stamp.check_point

    error_404 and return if sheet.blank? or stamp.blank? or !stamp.is_available or !stamp.add? or !check_point

    User.last.user_stamps.where('created_at > ?', Stamp.threshold_at + 1.day).pluck(:stamp_id).uniq.first

    unless Rails.env.local? or Rails.env.development?
      check_point_stamp_ids = check_point.stamps.pluck :id
      user_stamp_stamp_ids_created_at_today = @user.user_stamps.where('created_at > ?', Stamp.threshold_at).pluck(:stamp_id)

      user_stamp_stamp_ids_created_at_today.each do |stamp_id|
        valid = false if check_point_stamp_ids.include? stamp_id
      end
    end

    if valid
      position = @user.add_stamp stamp

      success( :valid => false ) unless position

      success(
        {
          :valid => valid,
          :user => {
            :token => @user.token
          },
          :sheet => {
            :stamp_position => position,
            :stamp_image_path => stamp.image_path,
            :api_sheet_check_condition_path => Rails.application.routes.url_helpers.api_sheet_check_condition_path,
            :token => sheet.token
          }
        }
      )
    else
      success( :valid => valid )
    end
  end
end
