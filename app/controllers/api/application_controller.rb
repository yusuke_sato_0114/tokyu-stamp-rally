class Api::ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  def error_404 data = {}
    error 404, data
  end

  def error_500 data = {}
    error 500, data
  end

  def success data = {}
    format 200, data
  end

  def error status, data = {}
    format status, data
  end

  def format status, data
    render(
      json: {
        status: status,
        data:   data
      },
      status: 200
    )
  end

  private
  def set_current_user
    @user = User.where(:token => params[:user][:token]).first
    error_404 and return if @user.blank?
  end

  def set_twitter_client
    @twitter_client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV["TWITTER_CONSUMER_KEY"]
      config.consumer_secret     = ENV["TWITTER_CONSUMER_SECRET"]
      config.access_token        = current_user.twitter_access_token
      config.access_token_secret = current_user.twitter_secret_token
    end
  end

  def login_required
    error(401, { :new_user_session_path => Rails.application.routes.url_helpers.new_user_session_path }) and return unless user_signed_in?
  end
end
