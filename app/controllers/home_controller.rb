class HomeController < ApplicationController
  before_action :login_required,           :except => [ :closed ]
  before_action :enquete_required,         :except => [ :closed ]
  before_action :set_current_sheet,        :only   => [ :index ]
  before_action :set_completed_sheet,      :only   => [ :sheet ]
  before_action :set_coupons,              :only   => [ :index ]
  before_action :set_unread_notifications, :only   => [ :index ]
  before_action :set_access_time,          :only   => [ :index ]

  def index
  end

  def sheet
  end

  def ranking
    @ranking = TopRanking.season.order(:rank).limit(Settings.top_ranking.display_rank_length)
    @ranking_users = User.find(@ranking.map {|r| r.user_id}).map{ |u| [u.id, { name: u.name, theme: u.theme }] }.to_h

    my_ranking = SelfRanking.season.where(:owner => current_user.id).first
    @my_ranking_users = my_ranking.arround_me if my_ranking
  end

  def lottery
  end

  def closed
    sign_out current_user if signed_in?
  end

  private
  def set_current_sheet
    @current_sheet = current_user.sheets.current_phase.includes(:stamps).current
    @current_sheet = current_user.create_new_sheet if @current_sheet.blank?
  end

  def set_completed_sheet
    @completed_sheets = current_user.sheets.completed
  end

  def set_coupons
    @coupons = []
    Coupon.current_phase.ordered.each do |coupon|
      user_coupon = current_user.user_coupons.where(:coupon_id => coupon.id).first

      if user_coupon.present?
        count = user_coupon.number
        image_path = coupon.list_image_path
      else
        count = 0
        image_path = coupon.list_blank_image_path
      end

      @coupons.push(
        {
          :count      => count,
          :path       => Rails.application.routes.url_helpers.coupon_path(coupon.token),
          :image_path => image_path,
          :present    => user_coupon.present?
        }
      )
    end
  end

  def set_unread_notifications
    if current_user.last_accessed_at.nil?
      @unread_notifications = @notifications
    else
      @unread_notifications = @notifications.where('published_at > ?', current_user.last_accessed_at)
    end
  end

  def set_access_time
    current_user.update_attributes! :last_accessed_at => Time.now
  end
end
