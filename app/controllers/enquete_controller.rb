class EnqueteController < ApplicationController
  before_action :login_required
  before_action :set_user, :only => [ :answer ]
  before_action :already_answered, :except => [ :thanks ]

  def new
    @user = current_user
  end

  def answer
    @user.assign_attributes(user_params)

    if @user.valid?
      @user.update_attributes! :answered_enquete => true
      redirect_to thanks_enquete_path
    else
      render 'new'
    end
  end

  def thanks
  end

  private
  def set_user
    @user = User.where(:id => params[:id]).first

    redirect_to new_enquete_path if @user.blank?
  end

  def user_params
    params.require(:user).permit(:serial_code, :name, :birthyear, :gender, :theme, :zip_code, :frequency_of_use_tokyu, :job, :agreement)
  end

  def already_answered
    redirect_to root_path if current_user.answered_enquete
  end
end
