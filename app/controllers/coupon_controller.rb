class CouponController < ApplicationController
  before_action :login_required
  before_action :set_coupon
  before_action :redirect_if_user_dont_have_coupon, :only => [ :show ]
  before_action :enquete_required

  def show
  end

  def used
  end

  private
  def set_coupon
    coupon = Coupon.where(:token => params[:token]).first
    redirect_to root_path and return if coupon.blank?
    @user_coupon = current_user.user_coupons.where(:coupon_id => coupon.id).first
    redirect_to root_path and return if @user_coupon.blank?
    @coupon = @user_coupon.coupon
  end

  def redirect_if_user_dont_have_coupon
    redirect_to root_path and return if @user_coupon.number == 0
  end
end
