class ConditionController < ApplicationController
  before_action :login_required
  before_action :set_condition
  before_action :enquete_required

  def show
  end

  private
  def set_condition
    @condition = Condition.where(:token => params[:token]).first
    redirect_to root_path and return if @condition.blank?
  end
end
