module ErrorHandlers
  extend ActiveSupport::Concern

  included do
    rescue_from Exception, with: :rescue500
    rescue_from ActionController::RoutingError, with: :rescue404
    rescue_from ActiveRecord::RecordNotFound, with: :rescue404
  end

  private
  def rescue404 e
    @exception = e
    # render 'errors/404', status: 404
    redirect_to root_path
  end

  def rescue500 e
    logger.error e.message if e.present?
    @exception = e
    # render '/errors/500', status: 500
    redirect_to root_path
  end
end
