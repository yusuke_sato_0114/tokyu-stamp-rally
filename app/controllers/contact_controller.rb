class ContactController < ApplicationController
  def new
    @contact = Contact.new
  end

  def confirm
    @contact = Contact.new contact_params

    render :new and return unless @contact.valid?
  end

  def complete
    @contact = Contact.new contact_params

    render :new and return if params[:back]

    ContactMailer.create_new(@contact).deliver
    ContactMailer.received(@contact).deliver
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :name_kana, :email, :message)
  end
end
