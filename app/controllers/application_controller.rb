class ApplicationController < ActionController::Base
  before_action :redirect_osokyu_lp, unless: :admin?
  before_action :set_notifications, unless: :admin?
  before_action :redirect_maintenance_page, if: 'Settings.in_maintenance'
  before_action :redirect_unsupported_page, if: :access_from_pc?

  layout :layout_handler

  include ErrorHandlers if Rails.env.production? or Rails.env.staging?

  helper_method :current_user

  private
  def admin?
    request.path =~ /^\/admin/
  end

  def redirect_osokyu_lp
    redirect_to 'http://www.tokyu.co.jp/osokyusan/index.html' unless Rails.env.development? or Rails.env.local?
  end

  def allowed_ip?
    return true if Rails.env.local?

    if Rails.env.production?
      remote_ip = request.env["HTTP_X_REAL_IP"]
    else
      remote_ip = request.env["HTTP_X_FORWARDED_FOR"] || request.remote_ip
    end

    Settings.allowed_ip.include? remote_ip
  end

  def layout_handler
    admin? ? 'admin' : 'application'
  end

  def access_from_pc?
    !request.smart_phone? and !request.tablet?
  end

  def redirect_maintenance_page
    redirect_to '/maintenance.html' unless allowed_ip?
  end

  def redirect_unsupported_page
    redirect_to '/unsupported.html'
  end

  def redirect_not_opened_page
    redirect_to '/not_opened.html'
  end

  def login_required
    redirect_to new_user_session_path unless user_signed_in?
  end

  def enquete_required
    redirect_to new_enquete_path unless current_user.answered_enquete
  end

  def set_notifications
    @notifications = Notification.published.from_new
  end
end
