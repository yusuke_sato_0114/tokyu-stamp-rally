class LotteryController < ApplicationController
  before_action :login_required
  before_action :enquete_required
  before_action :redirect_not_opened, :except => [ :not_yet ]
  before_action :set_lottery_prizes, :only => [ :top ]
  before_action :set_present_ticket, :only => [ :top, :ready, :cast ]
  before_action :redirect_top_if_no_ticket, :only => [ :ready, :cast ]
  before_action :set_lottery_prize, :only => [ :ready, :cast, :win, :lose ]
  before_action :set_lottery_unique_serial, :only => [ :win ]

  def top
  end

  def ready
  end

  def cast
    # rest_prize_count = @lottery_unique_serials.count
    # all_user_ticket_count = @present_coupon.user_coupons.sum :number

    # cast = rand(all_user_ticket_count)
    # cast = rand(20)
    # result = cast <= rest_prize_count
    cast = rand 10000
    result = cast <= (@lottery_prize.rate * 10000)
    # @present_ticket.decrement! :number, 1

    if result
      # lus = @lottery_unique_serials.first.code_for_params
      # @lottery_unique_serials.first.update_attributes!(
      #   :is_already_used => true,
      #   :user_id         => current_user.id
      # )
      redirect_to win_lottery_path(:lus => "this,code_is,dummy", :lp_id => @lottery_prize.id)
    else
      redirect_to lose_lottery_path(:lp_id => @lottery_prize.id)
    end
  end

  def win
  end

  def lose
  end

  def not_yet
    redirect_to lottery_path if Settings.lottery.is_open
  end

  private
  def redirect_not_opened
    redirect_to not_yet_lottery_path unless Settings.lottery.is_open
  end

  def set_lottery_prizes
    @lottery_prizes = LotteryPrize.stage(Settings.lottery.current_stage).ordered
  end

  def set_present_ticket
    @present_coupon = Coupon.present_ticket.first
    redirect_to root_path if @present_coupon.blank?

    @present_ticket = current_user.user_coupons.where(:coupon_id => @present_coupon.id).first
  end

  def redirect_top_if_no_ticket
    # redirect_to lottery_path if @present_ticket.blank? or @present_ticket.number == 0
  end

  def set_lottery_prize
    case action_name
    when 'ready', 'win', 'lose'
      lp_id = params[:lp_id]
    when 'cast'
      lp_id = params[:lottery][:lp_id]
    end

    redirect_to lottery_path and return if lp_id.blank?

    @lottery_prize = LotteryPrize.where(:id => lp_id).first
    redirect_to lottery_path and return if @lottery_prize.blank?

    @lottery_unique_serials = @lottery_prize.lottery_unique_serials.not_used
  end

  def set_lottery_unique_serial
    lus_params = params[:lus].split(',')
    # redirect_to lottery_path and return if lus_params.length != 3

    # @lottery_unique_serial = LotteryUniqueSerial.used.where(
    #   :lottery_id   => lus_params[0],
    #   :lottery_pass => lus_params[1],
    #   :lottery_code => lus_params[2]
    # ).first

    # redirect_to lottery_path and return if @lottery_unique_serial.blank?
    # redirect_to lottery_path and return if current_user.lottery_unique_serials.exclude? @lottery_unique_serial
    # redirect_to lottery_path and return unless @lottery_prize.lottery_unique_serials.include? @lottery_unique_serial

    @lus          = params[:lus]
    @lottery_id   = lus_params[0]
    @lottery_pass = lus_params[1]
    @lottery_code = lus_params[2]
  end
end
