class Users::PasswordsController < Devise::PasswordsController
  skip_before_action :require_no_authentication, :only => [ :new ]

  # GET /resource/password/new
  def new
    sign_out resource if signed_in?
    super
  end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  # def update
  #   super
  # end

  def instructions     
  end

  def reset
  end
  
  protected

  def after_resetting_password_path_for(resource)
    reset_user_password_path
    # super(resource)
  end

  # The path used after sending reset password instructions
  def after_sending_reset_password_instructions_path_for(resource_name)
    instructions_user_password_path
    # super(resource_name)
  end
end
