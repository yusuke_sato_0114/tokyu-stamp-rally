class SelfRanking < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  enum type: { :season => 1, :weekly => 2 }

  def self.update
    target_users = User.available.answered_enquete

    season_stamp_ranking = target_users.get_stamp_ranking Settings.self_ranking.season_start
    weekly_stamp_ranking  = target_users.get_stamp_ranking threshold_weekly_at

    set_ranking season_stamp_ranking, 1
    set_ranking weekly_stamp_ranking, 2
  end

  def self.set_ranking ranking, type
    where(:type => type).destroy_all
    self_rankings = []

    ranking.each.with_index(0) do |rank_data, i|
      s_r = new(
        :type              => type,
        :index             => i,
        :owner             => rank_data[:user_id],
        :owner_rank        => rank_data[:rank],
        :owner_stamp_count => rank_data[:stamp_count]
      )

      1.upto(Settings.self_ranking.range_limit) do |j|
        if i - j >= 0
          s_r.assign_attributes(
            "prev_#{j}".to_sym             => ranking[i - j][:user_id],
            "prev_#{j}_rank".to_sym        => ranking[i - j][:rank],
            "prev_#{j}_stamp_count".to_sym => ranking[i - j][:stamp_count]
          )
        end

        if i + j < ranking.count - 1
          s_r.assign_attributes(
            "next_#{j}".to_sym             => ranking[i + j][:user_id],
            "next_#{j}_rank".to_sym        => ranking[i + j][:rank],
            "next_#{j}_stamp_count".to_sym => ranking[i + j][:stamp_count]
          )
        end
      end

      self_rankings << s_r
    end

    import self_rankings
  end

  def arround_me
    users = User.find([prev_1, prev_2, prev_3, prev_4, prev_5, owner, next_1, next_2, next_3, next_4, next_5]).map{ |u| [u.id, { name: u.name, theme: u.theme }] }.to_h
    result = []
    rank_determined = false

    unless prev_1 or rank_determined
      arround_ids = { owner:  owner, next_1: next_1, next_2: next_2, next_3: next_3, next_4: next_4 }
      rank_determined = true
    end

    unless next_1 or rank_determined
      arround_ids = { prev_4: prev_4, prev_3: prev_3, prev_2: prev_2, prev_1: prev_1, owner:  owner }
      rank_determined = true
    end

    unless prev_2 or rank_determined
      arround_ids = { prev_1: prev_1, owner:  owner, next_1: next_1, next_2: next_2, next_3: next_3 }
      rank_determined = true
    end

    unless next_2 or rank_determined
      arround_ids = { prev_3: prev_3, prev_2: prev_2, prev_1: prev_1, owner:  owner, next_1: next_1 }
      rank_determined = true
    end

    unless rank_determined
      arround_ids = { prev_2: prev_2, prev_1: prev_1, owner:  owner, next_1: next_1, next_2: next_2 }
    end

    arround_ids.each do |key, id|
      result.push(
        {
          name:        users[id][:name],
          theme:       users[id][:theme],
          stamp_count: self["#{key}_stamp_count".to_sym],
          rank:        self["#{key}_rank".to_sym]
        }
      )
    end

    result
  end

  def self.threshold_weekly_at
    threshold_date = 1.week.ago
    Time.mktime threshold_date.year, threshold_date.month, threshold_date.day
  end
end
