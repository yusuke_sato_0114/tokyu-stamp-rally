class Session < ActiveRecord::Base
  def self.sweep time = 1.day, old = 2.days
    delete_all "updated_at < '#{time.ago.to_s(:db)}' OR created_at < '#{old.ago.to_s(:db)}'"
  end
end
