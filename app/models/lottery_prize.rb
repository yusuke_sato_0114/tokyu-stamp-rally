class LotteryPrize < ActiveRecord::Base
  has_many :lottery_unique_serials
  has_many :users, :through => :lottery_unique_serials

  scope :available, -> { where(:is_available => true) }
  scope :stage,     -> (stage) { where(:stage => stage) }
  scope :ordered,   -> { order(:order_index) }

  def win_image_path
    "#{Settings.lottery.result_image_dir}/#{grade.downcase}.jpg"
  end

  def win_image_url
    "#{Settings.aws.s3.host}#{win_image_path}"
  end
end
