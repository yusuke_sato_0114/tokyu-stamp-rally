class Notification < ActiveRecord::Base
  just_define_datetime_picker :published_at

  scope :published, -> { where(:is_published => true).where('published_at < ?', Time.now) }
  scope :from_new,  -> { order('published_at desc') }
end
