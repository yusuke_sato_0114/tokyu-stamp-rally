# coding: utf-8
class Sheet < ActiveRecord::Base
  belongs_to :user
  has_many :sheet_stamps, :dependent => :destroy
  has_many :stamps,       :through => :sheet_stamps

  include TokenGeneratorModule
  include ImageCompositeModule

  scope :current_phase, -> { where(:phase => Settings.phase) }
  scope :current,       -> { where(:is_current => true).last }
  scope :completed,     -> { where(:is_complete => true).order('completed_at desc') }

  after_initialize :generate_token, :if => :new_record?

  def check_condition
    Condition.stamp_count_equals_to(stamps.count).current_phase.order_by_high_priority.each do |condition|
      next if condition.sheet_count.present? and condition.sheet_count != number

      dumped_stamps = Marshal.load(Marshal.dump(stamps)).to_a

      dumped_condition_check_points = Marshal.load(Marshal.dump(condition.condition_check_points))
      dumped_condition_check_points = order_condition_check_points_by_priority dumped_condition_check_points

      dumped_condition_check_points.each do |c_c_p|
        if c_c_p.order # 順番指定があった場合
          if dumped_stamps.map { |stamp| stamp.order }.include? c_c_p.order
            target_stamp = dumped_stamps.select { |stamp| stamp.order == c_c_p.order }[0]
          else
            break
          end

          if c_c_p.is_unspecified # スタンプ指定がない場合
            dumped_stamps = dumped_stamps.delete target_stamp
          else #スタンプ指定がある場合
            if target_stamp.check_point.id == c_c_p.check_point.id
              dumped_stamps.delete_at dumped_stamps.index(target_stamp)
            else
              break
            end
          end
        else # 順番指定がない場合
          if c_c_p.is_unspecified # スタンプ指定がない場合
            dumped_stamps.shift
          else # スタンプ指定がある場合
            sheet_check_point_ids = dumped_stamps.map { |stamp| stamp.check_point }.map { |check_point| check_point.id }

            if sheet_check_point_ids.include? c_c_p.check_point.id
              target_stamp = dumped_stamps.select { |stamp| stamp.check_point.id == c_c_p.check_point.id }[0]
              dumped_stamps.delete_at dumped_stamps.index(target_stamp)
            else
             break
            end
          end
        end
      end

      return condition if dumped_stamps.blank?
    end

    return nil
  end

  def is_complete?
    Settings.sheet.complete_count == stamps.count
  end

  def share_image_url
    stamp_numbers = []

    sheet_stamps.each do |sheet_stamp|
      stamp_numbers << sheet_stamp.stamp.number
    end

    "#{Settings.aws.s3.host}#{Settings.sheet.share_image_path}/#{phase}/#{stamp_numbers.join '_'}.png"
  end

  private
  def order_condition_check_points_by_priority condition_check_points
    ordered = []

    ordered += condition_check_points.select { |c_c_p| not c_c_p.is_unspecified and c_c_p.order }
    ordered += condition_check_points.select { |c_c_p| not c_c_p.is_unspecified and not c_c_p.order }
    ordered += condition_check_points.select { |c_c_p| c_c_p.is_unspecified and c_c_p.order }
    ordered += condition_check_points.select { |c_c_p| c_c_p.is_unspecified and not c_c_p.order }

    ordered
  end
end
