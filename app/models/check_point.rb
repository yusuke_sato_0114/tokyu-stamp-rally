class CheckPoint < ActiveRecord::Base
  has_many :condition_check_points, :dependent => :destroy
  has_many :conditions,             :through => :condition_check_points
  has_many :stamps
  accepts_nested_attributes_for :stamps, allow_destroy: true
end
