class SheetStamp < ActiveRecord::Base
  belongs_to :sheet
  belongs_to :stamp
end
