class LotteryUniqueSerial < ActiveRecord::Base
  belongs_to :user
  belongs_to :lottery_prize
  before_create :set_number

  scope :not_used, -> { where(:is_already_used => false) }
  scope :used,     -> { where(:is_already_used => true) }

  def set_number
    lottery_prize = LotteryPrize.where(:id => lottery_prize_id).first
    return if lottery_prize.blank?

    last_number = lottery_prize.lottery_unique_serials.present? ? lottery_prize.lottery_unique_serials.maximum(:number) : 0
    assign_attributes :number => last_number + 1
  end

  def code_for_params
    "#{lottery_id},#{lottery_pass},#{lottery_code}"
  end
end
