# coding: utf-8
class Contact
  include ActiveModel::Model

  attr_accessor :name, :name_kana, :email, :message

  validates_presence_of :name, message: 'お名前を入力してください'
  validates_presence_of :name_kana, message: 'お名前(カナ)を入力してください'
  validates_presence_of :email, message: 'メールアドレスを入力してください'
  validates_presence_of :message, message: 'お問い合わせ内容を入力してください'
end
