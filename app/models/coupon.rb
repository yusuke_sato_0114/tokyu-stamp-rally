class Coupon < ActiveRecord::Base
  has_many :user_coupons, :dependent => :destroy
  has_many :users,        :through => :user_coupons
  has_many :conditions
  has_many :stamps

  self.inheritance_column = :_type_disabled

  include TokenGeneratorModule

  enum type: { :normal => 10, :present_ticket => 20 }

  scope :current_phase, -> { where(:phase => Settings.phase) }
  scope :ordered,   -> { order(:index_order) } 

  after_initialize :generate_token, :if => :new_record?

  def list_image_path
    "#{Settings.coupon.list_image_dir}/#{Settings.phase}/#{list_image_filename}.#{Settings.coupon.list_image_extension}"
  end

  def list_blank_image_path
    "#{Settings.coupon.list_blank_image_dir}/#{Settings.phase}/#{list_blank_image_filename}.#{Settings.coupon.list_blank_image_extension}"
  end

  def detail_image_path
    "#{Settings.coupon.detail_image_dir}/#{Settings.phase}/#{detail_image_filename}.#{Settings.coupon.detail_image_extension}"
  end
end
