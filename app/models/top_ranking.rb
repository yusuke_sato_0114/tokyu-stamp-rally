class TopRanking < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  enum type: { :season => 1, :weekly => 2 }

  def self.update
    target_users = User.available.answered_enquete

    season_stamp_ranking = target_users.get_stamp_ranking(Settings.top_ranking.season_start).slice(0,Settings.top_ranking.rank_length)
    weekly_stamp_ranking  = target_users.get_stamp_ranking(threshold_weekly_at).slice(0,Settings.top_ranking.rank_length)

    set_ranking season_stamp_ranking, 1
    set_ranking weekly_stamp_ranking, 2
  end

  def self.set_ranking ranking, type
    where(:type => type).destroy_all
    top_rankings = []
    
    ranking.each.with_index(0) do |rank_data, i|
      t_r = new(
        :type        => type,
        :index       => i,
        :user_id     => rank_data[:user_id],
        :rank        => rank_data[:rank],
        :stamp_count => rank_data[:stamp_count]
      )

      top_rankings << t_r
    end

    import top_rankings
  end

  def self.threshold_weekly_at
    threshold_date = 1.week.ago
    Time.mktime threshold_date.year, threshold_date.month, threshold_date.day
  end
end
