module TokenGeneratorModule
  extend ActiveSupport::Concern

  def generate_token
    assign_attributes( :token => self.class.get_token )
  end

  module ClassMethods
    def get_token
      try_count = 5
      1.upto(try_count) do
        token = SecureRandom.urlsafe_base64 30
        return token if where(:token => token).first.nil?
      end
      logger.error 'Duplicate token error'
    end
  end
end
