module ImageCompositeModule
  require 'rmagick'
  extend ActiveSupport::Concern

  module ClassMethods
    def generate_image
      1.upto(7) do |i|
        1.upto(7) do |j|
          1.upto(7) do |k|
            1.upto(7) do |l|
              1.upto(7) do |m|
                1.upto(7) do |n|
                  image = get_blob "#{Rails.root}/data/sheet_image/base/base.png"

                  image = image.composite get_character_blob(i), 21,  22,  Magick::OverCompositeOp
                  image = image.composite get_character_blob(j), 242, 22,  Magick::OverCompositeOp
                  image = image.composite get_character_blob(k), 465, 22,  Magick::OverCompositeOp
                  image = image.composite get_character_blob(l), 21,  245, Magick::OverCompositeOp
                  image = image.composite get_character_blob(m), 242, 245, Magick::OverCompositeOp
                  image = image.composite get_character_blob(n), 465, 245, Magick::OverCompositeOp

                  puts "generated #{i}_#{j}_#{k}_#{l}_#{m}_#{n}"

                  image.write image_path [i,j,k,l,m,n]
                end
              end
            end
          end
        end
      end
    end

    def image_path arr
      "#{Rails.root}/public/static/share/2/#{arr[0]}_#{arr[1]}_#{arr[2]}_#{arr[3]}_#{arr[4]}_#{arr[5]}.png"
    end

    def get_character_blob num
      get_blob "#{Rails.root}/data/sheet_image/base/2/#{num}.png"
    end

    def get_blob path
      Magick::Image.from_blob(File.read(path)).first
    end
  end
end
