class UniqueSerial < ActiveRecord::Base
  scope :not_used, -> { where(:is_already_used => false) }
  scope :used,     -> { where(:is_already_used => true) }
end
