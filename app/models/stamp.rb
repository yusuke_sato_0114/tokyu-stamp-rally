class Stamp < ActiveRecord::Base
  belongs_to :check_point
  belongs_to :coupon
  has_many :sheet_stamps, :dependent => :destroy
  has_many :sheets, :through => :sheet_stamps
  has_many :user_stamps, :dependent => :destroy
  has_many :users, :through => :user_stamps

  scope :available, -> { where(:is_available => true) }

  self.inheritance_column = :_type_disabled

  enum type: { :both => 10, :add => 20, :use => 30 }

  def self.threshold_at
    now = Time.now
    Time.mktime(now.year, now.month, now.day, 0, 0, 0)
  end

  def image_path
    "#{Settings.stamp.image_dir}/#{Settings.phase}/#{image_filename}.#{Settings.stamp.image_extension}"
  end
end
