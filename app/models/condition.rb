class Condition < ActiveRecord::Base
  has_many   :condition_check_points, :dependent => :destroy
  has_many   :check_points,           :through => :condition_check_points
  belongs_to :coupon

  scope :sheet_count_equals_to,  -> (count) { where(:sheet_count => count) }
  scope :stamp_count_equals_to,  -> (count) { where(:stamp_count => count) }
  scope :current_phase,          -> { where(:phase => Settings.phase) }
  scope :order_by_high_priority, -> { order('priority desc') }

  include TokenGeneratorModule

  after_initialize :generate_token, :if => :new_record?

  def image_path
    "#{Settings.condition.image_dir}/#{image_filename}.#{Settings.condition.image_extension}"
  end

  def audio_path
    "#{Settings.condition.audio_dir}/#{audio_filename}.#{Settings.condition.audio_extension}"
  end
end
