# coding: utf-8
class User < ActiveRecord::Base
  has_many :sheets, :dependent => :destroy
  has_many :user_coupons, :dependent => :destroy
  has_many :coupons, :through => :user_coupons
  has_many :user_stamps, :dependent => :destroy
  has_many :stamps, :through => :user_stamps
  has_many :lottery_unique_serials
  has_many :lottery_prizes, :through => :lottery_unique_serials

  devise :database_authenticatable, :registerable,
         :recoverable, :validatable, :confirmable,
         :omniauthable, omniauth_providers: [:twitter]

  include TokenGeneratorModule

  scope :available, -> { where(:is_available => true) }
  scope :answered_enquete, -> { where(:answered_enquete => true) }

  enum gender: { :male => 1, :female => 2}

  enum theme: {
         :osomatsu    => 1,
         :karamatsu   => 2,
         :tyoromatsu  => 3,
         :ichimatsu   => 4,
         :juushimatsu => 5,
         :todomatsu   => 6,
         :iyami       => 7
       }

  enum job: {
         :businessman                             => 10,
         :public_officer                          => 20,
         :self_employed                           => 30,
         :housemanager                            => 40,
         :part_timer                              => 50,
         :student                                 => 60,
         :vocational_school_or_unviersity_student => 70,
         :any_other_students                      => 80,
         :neet_or_unemployed                      => 90,
         :others                                  => 100
       }

  enum frequency_of_use_tokyu: {
         :everyday                       => 10,
         :two_three_per_a_week           => 20,
         :one_per_a_week                 => 30,
         :two_three_per_a_month          => 40,
         :one_per_a_month                => 50,
         :one_per_two_three_months       => 60,
         :less_than_one_per_three_months => 70,
         :the_first_time                 => 80
       }

  validate :serial_code_should_be_valid_by_email, on: :update
  validate :serial_code_should_be_valid_by_twitter, on: :update
  validates_acceptance_of :agreement, message: '利用規約・個人情報保護法に同意する場合チェックを入れてください', on: :update, unless: :reset_password_token
  validates :email, confirmation: true
  # validates_presence_of :name, message: 'ニックネームを入力してください', on: :update
  validates_length_of :name, in: 3..16, message: 'ニックネームは3文字以上16文字以内です', on: :update, unless: :reset_password_token
  validates_presence_of :birthyear, message: '誕生年を入力してください', on: :update, unless: :reset_password_token
  # validates_numericality_of :birthyear, 
  #                           only_integer: true,
  #                           greater_than_or_equal_to: 1950,
  #                           less_than_or_equal_to: 2010,
  #                           message: "誕生年は1950年から2010年の範囲で入力してください",
  #                           on: :update
  validates_presence_of :gender, message: '性別を入力してください', on: :update, unless: :reset_password_token
  # validates_inclusion_of :gender, in: User.genders.keys, message: '性別は男か女で入力してください', on: :update
  validates_presence_of :theme, message: '推し急を入力してください', on: :update, unless: :reset_password_token
  # validates_inclusion_of :theme, in: User.themes.keys, message: '推し急は正しく入力してください', on: :update
  validates_presence_of :frequency_of_use_tokyu, message: '東急の利用頻度を入力してください', on: :update, unless: :reset_password_token
  # validates_inclusion_of :frequency_of_use_tokyu, in: User.frequency_of_use_tokyus.keys, message: '東急の利用頻度は正しく入力してください', on: :update
  validates_presence_of :job, message: '職業を入力してください', on: :update, unless: :reset_password_token
  # validates_inclusion_of :job, in: User.jobs.keys, message: '職業は正しく入力してください', on: :update
  # validates_presence_of :zip_code, message: '郵便番号を入力してください', on: :update
  validates_format_of :zip_code, :with => /\A[0-9]{7}\Z/, message: '郵便番号は半角数字7桁(ハイフンなし)で入力してください', on: :update, unless: :reset_password_token
  
  after_initialize :generate_token, :if => :new_record?
  after_create     :create_new_sheet
  after_destroy    :restore_serial_code

  def serial_code_should_be_valid_by_email
    return if provider == 'twitter'
    return if is_authenticated_by_serial

    if serial_code.blank?
      errors.add :serial_code, 'シリアルナンバーを入力してください'
    else
      match = UniqueSerial.not_used.where(:code => serial_code).first

      if match.blank?
        errors.add :serial_code, 'シリアルナンバーが間違っています'
      else
        assign_attributes :is_authenticated_by_serial => true
        match.update_attributes! :is_already_used => true if valid?
      end
    end
  end

  def serial_code_should_be_valid_by_twitter
    return if provider == 'email'
    return if is_authenticated_by_serial

    if serial_code.blank?
      errors.add :serial_code, 'シリアルナンバーを入力してください'
    else
      match = UniqueSerial.not_used.where(:code => serial_code).first

      if match.blank?
        errors.add :serial_code, 'シリアルナンバーが間違っています'
      else
        assign_attributes :is_authenticated_by_serial => true
        match.update_attributes! :is_already_used => true if valid?
      end
    end
  end

  def create_new_sheet
    number_in_phase = sheets.current_phase.count == 0 ? 1 : sheets.current_phase.maximum(:number) + 1
    number = sheets.completed.count == 0 ? 1 : sheets.completed.count + 1

    sheet = sheets.create!(
      :name   => Settings.sheet.default_name.gsub(/\{\{number\}\}/, number.to_s),
      :number => number_in_phase,
      :phase  => Settings.phase
    )

    sheet
  end

  def restore_serial_code
    match = UniqueSerial.used.where(:code => serial_code).first
    return if match.blank?

    match.update_attributes! :is_already_used => false
  end

  def get_coupon condition
    coupon = condition.coupon
    user_coupon = user_coupons.where(:coupon_id => coupon.id).first

    if user_coupon.present?
      user_coupon.increment! :number, coupon.get_count
    else
      user_coupon = user_coupons.create! :coupon_id => coupon.id, :number => coupon.get_count
    end

    {
      :information => coupon,
      :count       => user_coupon.number
    }
  end

  def add_stamp stamp
    sheet = sheets.current
    return false if sheet.blank?

    position = sheet.sheet_stamps.blank? ? 1 : sheet.sheet_stamps.last.position + 1
    return false if position > Settings.sheet.complete_count

    sheet.sheet_stamps.create!(:stamp_id => stamp.id, :position => position)

    if sheet.is_complete?
      sheet.update_attributes! :is_current => false, :is_complete => true, :completed_at => Time.now
      create_new_sheet
    end

    stamps << stamp
    stamp.increment! :use_count, 1

    position
  end

  def self.find_for_oauth auth
    user = where(uid: auth.uid, provider: auth.provider).first

    unless user
      user = User.new(
        uid:                  auth.uid,
        provider:             auth.provider,
        twitter_screen_name:  auth.info.nickname,
        twitter_access_token: auth.credentials.token,
        twitter_secret_token: auth.credentials.secret
      )

      user.skip_confirmation!
      user.save!
    end

    user
  end

  def update_access_token auth
    update_attributes!(
      uid:                  auth.uid,
      twitter_screen_name:  auth.info.nickname,
      twitter_access_token: auth.credentials.token,
      twitter_secret_token: auth.credentials.secret
    )
  end

  def self.get_stamp_ranking threshold_from
    user_stamp_data = []
    stamp_count_data = []

    available.each do |user|
      stamp_count = user.user_stamps.where('created_at > ?', threshold_from).count

      user_stamp_data.push({ :user_id => user.id, :stamp_count => stamp_count })
      stamp_count_data.push stamp_count
    end

    user_stamp_data.map do |u_s|
      u_s[:rank] = stamp_count_data.count { |c| c > u_s[:stamp_count] } + 1
    end

    user_stamp_data.sort_by { |u_s| u_s[:rank] }
  end

  def self.get_count_by_created_at
    start_at = Time.parse Settings.site.start_at.to_s
    res = []
    days = ((Time.now - start_at) / 60 / 60 / 24).to_i + 1

    days.times do |i|
      res.push(
        {
          :created_at => (start_at + i.days).to_s(:date),
          :user_count => available.answered_enquete.where('created_at > ?', start_at).where('created_at < ?', start_at + (i + 1).days).count
        }
      )
    end

    res
  end

  def email_required?
    super && provider == 'email'
  end

  def password_required?
    super && provider == 'email'
  end

  def all_stamps
    user_stamps.count
  end

  def avg_stamps
    start_at = Time.mktime(2016,8,1)
    days = ((Time.mktime(2016,10,1) - Time.mktime(2016,8,1)) / 60 / 60 / 24).to_i + 1
    res = []
    ret = {
      :avg => 0,
      :day_count => 0
    }

    days.times do |i|
      res.push user_stamps.where('created_at > ?',start_at + i.days).where('created_at < ?',start_at + (i + 1).days).count
      res.pop if res.last == 0
    end

    return ret if res.size == 0
    avg = res.inject(0.0){|r,i| r+=i} / res.size
    ret[:avg] = avg.round(1)
    ret[:day_count] = res.size

    ret
  end

  def self.get_daily_summary_user_stamp_data
    start_at = Time.mktime(2016,8,1)
    days = ((Time.mktime(2016,10,1) - Time.mktime(2016,8,1)) / 60 / 60 / 24).to_i + 1

    csv = CSV.generate do |csv|
      days.times do |i|
        daily_summary_user_stamp = UserStamp.where('created_at >= ?',start_at + i.days).where('created_at < ?',start_at + (i + 1).days).count
        summary_user_stamp = UserStamp.where('created_at < ?', start_at + (i + 1).days).count
        csv << [(start_at + i.days).to_s(:date), daily_summary_user_stamp, summary_user_stamp]
      end
    end

    File.open("#{Rails.root}/data/daily_summary_user_stamp_data.csv", 'w') do |file|
      file.write(csv)
    end
  end

  def self.get_user_stamp_data from, to
    csv = CSV.generate do |csv|
      # csv << ['ID','名前','合計スタンプ数','平均スタンプ数','スタンプ押下日数']

      answered_enquete.where('id > ?', from).where('id < ?', to).each do |user|
        avg_data = user.avg_stamps
        csv << [user.id, user.name, user.all_stamps, avg_data[:avg], avg_data[:day_count]]
      end
    end

    File.open("#{Rails.root}/data/user_stamp_data.csv", 'a') do |file|
      file.write(csv)
    end
  end
end
