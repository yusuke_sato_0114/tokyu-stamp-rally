class UserStamp < ActiveRecord::Base
  belongs_to :user
  belongs_to :stamp
end
