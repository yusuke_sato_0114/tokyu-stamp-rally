ApplicationController = window.TokyuStampRally.ApplicationController
actions = window.TokyuStampRally.actions

controller_action = "#{$('.js-element__global-wrapper').data('controller')}_#{$('.js-element__global-wrapper').data('action')}"

if _.includes _.keys(actions), controller_action
  action = new actions[controller_action]
else
  action = new ApplicationController

action.init()
