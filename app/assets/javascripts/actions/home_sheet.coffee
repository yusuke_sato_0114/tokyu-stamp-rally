ApplicationController = window.TokyuStampRally.ApplicationController

window.TokyuStampRally.actions.home_sheet = class HomeSheet extends ApplicationController
  init: ->
    super()

    $ '.js-element__global-wrapper'
      .on 'blur', '.js-element__input--sheet-name', _update_sheet_name
      .on 'ajax:success', '.js-element__form--sheet-name', _callback_update_sheet_name
      .on 'click', '.js-trigger__change-sheet-order', _change_sheet_order
      .on 'click', '.js-trigger__open-tweet-popup', (e) =>
        @open_tweet_popup ->
          $('.js-element__sheet-token').val $(@).data 'sheet-token'
      .on 'click', '.js-trigger__close-tweet-popup', (e) =>
        @close_tweet_popup ->
          $('.js-element__sheet-token').val ''

  _update_sheet_name = ->
    $(@)
      .parents '.js-element__form--sheet-name'
      .submit()

  _callback_update_sheet_name = (event, response, status) ->
    $(@)
      .parents '.js-element__sheet-item'
      .attr
        'data-name': response.data.sheet.name

  _change_sheet_order = ->
    order_information = $('.js-trigger__select-sheet-order').val()
    order_target      = order_information.split('_')[0]
    order_dir         = order_information.split('_')[1]
    dir               = if order_dir is 'asc' then -1 else 1

    sheets = []

    $('.js-element__sheet-item').each ->
      sheets.push $(@)

    sheets.sort ($el1, $el2) ->
      return 0 if $el1.data(order_target) is $el2.data(order_target)
      return if $el1.data(order_target) < $el2.data(order_target) then 1 * dir else -1 * dir

    $('.js-element__sheet-list')
      .empty()
      .append sheets
