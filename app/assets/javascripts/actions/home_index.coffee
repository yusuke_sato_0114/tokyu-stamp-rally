ApplicationController = window.TokyuStampRally.ApplicationController

window.TokyuStampRally.actions.home_index = class HomeIndex extends ApplicationController
  init: ->
    super()

    howler_sounds = _set_howler_sounds()

    if @rails_env is 'local'
      $('.js-trigger__push-stamp').on 'click', (e) =>
        stamp_id = $(e.target).data('stamp-id')
        @display_stamp_seal()
        @on_stamp stamp_id: stamp_id
        howler_sounds[_get_last_stamp_position()].play() # if _valid_stamp stamp_id
    else
      @initialize_stamp
        callback: (result) =>
          stamp_id = result.stamp
          @on_stamp stamp_id: stamp_id

          howler_sounds[_get_last_stamp_position()].play() # if _valid_stamp stamp_id

    $ '.js-element__global-wrapper'
      .on 'click', '.js-trigger__close-popup-unread-notifications', ->
        $('.js-element__popup--unread-notifications').removeClass 'state__display'

  on_stamp: (data) ->
    $.ajax
      url: '/api/stamp/push'
      type: 'POST'
      data:
        _method: "post"
        authenticity_token: $('meta[name="csrf-token"]').attr 'content'
        stamp:
          device_id: data.stamp_id
        user:
          token: @user_token
      success: (response, status, xhr) ->
        location.href = response.data.new_user_session_path if response.status is 401

        if response.data.valid
          sheet = response.data.sheet
          user  = response.data.user

          $(".js-element__stamp-wrapper[data-index='#{sheet.stamp_position}']")
            .append $("<img src='#{sheet.stamp_image_path}' data-position='#{sheet.stamp_position}' class='js-element__stamp-image'>")

          $.ajax
            url: sheet.api_sheet_check_condition_path,
            type: 'POST',
            data:
              sheet:
                token: sheet.token
              user:
                token: user.token
            success: (response, status, xhr) ->
              setTimeout ->
                location.href = response.data.condition_path if response.data.condition_path
              , 2000

  _audio_dir = $('.js-data__audio-dir').data 'audio-dir'

  _get_last_stamp_position = ->
    if $('.js-element__stamp-image').length is 0 then 0 else $('.js-element__stamp-image').last().data('position')

  _valid_stamp = (stamp_id) ->
    last_push_stamp_at = sessionStorage.getItem stamp_id
    now = new Date

    sessionStorage.setItem stamp_id, now
    return true unless last_push_stamp_at

    threshold_time = new Date now.getFullYear(),now.getMonth(),now.getDate()
    new Date(last_push_stamp_at) < threshold_time

  _set_howler_sounds = ->
    arr = []

    for i in [1..6]
      arr[i - 1] = new Howl urls: ["#{_audio_dir}#{i}.mp3?#{(new Date()).getTime()}"]

    arr

  _is_mobile_safari = ->
    ua = navigator.userAgent.toLowerCase()

    /ip/.test(ua) and /safari/.test(ua)
