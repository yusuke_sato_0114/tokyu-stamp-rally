ApplicationController = window.TokyuStampRally.ApplicationController

window.TokyuStampRally.actions.lottery_ready = class LotteryReady extends ApplicationController
  init: ->
    super()

    $('.js-trigger__cast-lottery').on 'click', ->
      $(@).off 'click'

      if _$animation.get(0).completed
        _submit_cast_form()
      else
        _$animation.on 'load', _submit_cast_form

      _$animation.attr src: _animation_gif_path

      setTimeout ->
        $('.js-element__form--cast-lottery').submit() unless _in_animation
      , 5000

  _$animation         = $('.js-element__animation-lottery-machine')
  _animation_gif_path = '/static/application/img/lottery/ready/lottery__ready__animation--lottery-machine.gif'
  _animation_duration = 3100
  _in_animation       = false

  _submit_cast_form = ->
    _in_animation = true

    setTimeout ->
      $('.js-element__form--cast-lottery').submit()
    , _animation_duration
