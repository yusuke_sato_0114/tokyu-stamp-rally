ApplicationController = window.TokyuStampRally.ApplicationController

window.TokyuStampRally.actions.coupon_show = class CouponUse extends ApplicationController
  init: ->
    super()

    if @rails_env is 'local'
      $('.js-trigger__use-stamp').on 'click', (e) =>
        @display_stamp_seal()
        @on_coupon_use stamp_id: $(e.target).data('stamp-id')
    else
      if @coupon_type is 'normal'
        @initialize_stamp
          callback: (result) =>
            @on_coupon_use stamp_id: result.stamp

  coupon_id:   $('.js-data__coupon-id').data 'coupon-id'
  coupon_type: $('.js-data__coupon-type').data 'coupon-type'

  on_coupon_use: (data) ->
    $.ajax
      url: '/api/coupon/use'
      type: 'POST'
      data:
        _method: "post"
        authenticity_token: $('meta[name="csrf-token"]').attr 'content'
        stamp:
          device_id: data.stamp_id
        coupon:
          id: @coupon_id
        user:
          token: @user_token
      success: (response, status, xhr) ->
        location.href = response.data.new_user_session_path if response.status is 401
        location.href = response.data.coupon.used_path if response.data.valid
