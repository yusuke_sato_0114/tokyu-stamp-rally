ApplicationController = window.TokyuStampRally.ApplicationController

window.TokyuStampRally.actions.lottery_win = class LotteryWin extends ApplicationController
  init: ->
    super()

    $ '.js-element__global-wrapper'
      .on 'click', '.js-trigger__open-tweet-popup', (e) =>
        @open_tweet_popup()
      .on 'click', '.js-trigger__close-tweet-popup', (e) =>
        @close_tweet_popup()
