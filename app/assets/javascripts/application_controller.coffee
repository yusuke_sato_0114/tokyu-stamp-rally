window.TokyuStampRally.ApplicationController = class ApplicationController
  init: ->
    $ '.js-element__global-wrapper'
      .on 'click', '.js-trigger__toggle-header-menu, .js-element__header-menu-overlay', _toggle_header_menu
      .on 'click', '.js-trigger__smooth-scroll', _smooth_scroll
      .on 'click', '.js-trigger__close-popup-error', _close_error_popup
      .on 'click', '.js-trigger__open-notifications-modal', ->
        _open_modal 'notifications'
      .on 'click', '.js-trigger__close-notifications-modal', ->
        _close_modal 'notifications'
      .on 'click', '.js-trigger__open-terms-modal', ->
        _open_modal 'terms'
      .on 'click', '.js-trigger__close-terms-modal', ->
        _close_modal 'terms'
      .on 'click', '.js-trigger__open-faq-modal', ->
        _open_modal 'faq'
      .on 'click', '.js-trigger__close-faq-modal', ->
        _close_modal 'faq'
      .on 'click', '.js-trigger__toggle-notifications-accordion', ->
        _toggle_modal_accordion.call @, 'notifications'
      .on 'click', '.js-trigger__close-notifications-accordion', ->
        _toggle_modal_accordion.call @, 'notifications'
      .on 'click', '.js-trigger__toggle-faq-accordion', ->
        _toggle_modal_accordion.call @, 'faq'
      .on 'click', '.js-trigger__close-faq-accordion', ->
        _toggle_modal_accordion.call @, 'faq'
      .on 'click', '.js-trigger__play-audio', _play_audio
      .on 'keyup', '.js-element__tweet-input', _count_tweet_text
      .on 'click', '.js-element__tweet-button', =>
        @tweet_from_popup()

  open_error_popup: (error) ->
    $ '.js-append-target__popup-error'
      .empty()
      .append _popup_error_template error: error
      .addClass 'state__display'

  rails_env:     $('.js-data__rails-env').data 'rails-env'
  user_token:    $('.js-data__user-token').data 'user-token'
  stamp_api_key: $('.js-data__stamp-api-key').data 'api-key'

  display_stamp_seal: ->
    $ '.js-element__stamp-seal-layer'
      .addClass 'state__display'
      .css
        opacity: 1

    setTimeout ->
      $ '.js-element__stamp-seal-layer'
        .animate
          opacity: 0
        , 300, ->
          $(@)
            .removeClass 'state__display'
    , 1500

  open_tweet_popup: (fn = undefined) ->
    $('.js-element__tweet-input').val _default_text
    $('.js-element__tweet-text-count').html _get_current_tweet_text_rest_count()
    $('.js-element__tweet-popup').addClass 'state__display'
    fn && fn()

  close_tweet_popup: (fn = undefined) ->
    $('.js-element__tweet-input').val ''
    $('.js-element__tweet-popup').removeClass 'state__display'
    $('.js-element__tweet-text-count')
    $('.js-element__tweet-button').removeClass 'state__unavailable'
    $('.js-element__tweet-text-count')
      .html ''
      .removeClass 'state__text-length-over'
    fn && fn()

  tweet_from_popup: ->
    return if _get_current_tweet_text_rest_count() < 0

    $('.js-element__tweet-form').submit()
    @close_tweet_popup()

  initialize_stamp: (data) ->
    echoss.setLanguageCode 'ja'
    echoss.initialize @stamp_api_key, echoss.REGION_CODE_TYPE.JAPAN

    echoss.initializeSuccess = ->
      echoss.Stamp.init ->
        console.log 'echoss init success'
      , (errorCode, errorMsg) ->
        console.log "echoss init failed. errorCode => #{errorCode}, errorMsg => #{errorMsg}"

    echoss.Stamp.onError = (errorCode, errorMsg) =>
      console.log "onError handler. errorCode => #{errorCode}, errorMsg => #{errorMsg}"

      # 端末を横向きにしたときのエラーは無視
      unless errorCode is 'S002'
        @open_error_popup
          title:   'エラー'
          content: '何らかの理由でスタンプを読み込めませんでした。はみ出さないように注意して、もう一度押してみてください。'

    echoss.Stamp.onStamp = (params) =>
      return unless @stamp_enabled
      @stamp_enabled = false
      @display_stamp_seal()

      echoss.Stamp.auth
        stampParams: params
      , (result) =>
        console.log result
        data.callback(result) if data.callback?
        @stamp_enabled = true
      , (errorCode, errorMsg) =>
        console.log "onStamp error handler. errorCode => #{errorCode}, errorMsg => #{errorMsg}"
        @open_error_popup
          title:   'エラー'
          content: '何らかの理由でスタンプを読み込めませんでした。はみ出さないように注意して、もう一度押してみてください。'
        @stamp_enabled = true

  stamp_enabled: true

  _close_error_popup = ->
    $ '.js-element__popup-error'
      .removeClass 'state__display'
      .empty()

  _toggle_header_menu = ->
    $ '.js-element__header-menu, .js-element__header-menu-overlay'
      .toggleClass 'state__display'

  _open_header_menu = ->
    $ '.js-element__header-menu, .js-element__header-menu-overlay'
      .addClass 'state__display'

  _close_header_menu = ->
    $ '.js-element__header-menu, .js-element__header-menu-overlay'
      .removeClass 'state__display'

  _smooth_scroll = (e)->
    e.preventDefault()

    $('html, body').animate
      scrollTop: $($(@).attr('href')).offset().top
    , 400, 'easeInOutCubic'

  _open_modal = (target) ->
    _close_header_menu()
    $ ".js-element__modal-#{target}"
      .addClass 'state__display'

  _close_modal = (target) ->
    $ ".js-element__modal-#{target}"
      .removeClass 'state__display'

  _toggle_modal_accordion = (target) ->
    $ @
      .parents ".js-element__#{target}-item"
      .children ".js-element__#{target}-accordion"
      .slideToggle 300

  _play_audio = (e) ->
    $audio = $($(@).data('audio-selector'))

    if e.target.tagName.toLowerCase() is 'a' and e.target.target isnt '_blank'
      e.preventDefault()

      $audio.on 'ended', ->
        location.href = e.target.href
    else if e.target.tagName.toLowerCase() is 'input' and $(e.target).attr('type') is 'submit'
      e.preventDefault()
      $audio.on 'ended', ->
        $(e.target).parents('form').submit()

    $audio.get(0).play()

  _popup_error_template = _.template('' +
    '<div class="popup--error__wrapper">' +
      '<p class="popup--error__title"><%= error.title %></p>' +
      '<p class="popup--error__content"><%= error.content %></p>' +
      '<p class="popup--error__button--close js-trigger__close-popup-error">とじる</p>' +
    '</div>'
  )

  _get_current_tweet_text_rest_count = ->
    _initial_editable_text_length - $('.js-element__tweet-input').val().length

  _count_tweet_text = ->
    current_rest_count = _get_current_tweet_text_rest_count()

    $('.js-element__tweet-text-count')
      .html current_rest_count

    if current_rest_count <= 0
      $('.js-element__tweet-text-count').addClass 'state__text-length-over'
      $('.js-element__tweet-button').addClass 'state__unavailable'
    else
      $('.js-element__tweet-text-count').removeClass 'state__text-length-over'
      $('.js-element__tweet-button').removeClass 'state__unavailable'

  _default_text = $('.js-element__tweet-input').data 'default-text'
  _tweet_text_max_length = 140
  _image_url_length = 24
  _site_url_length = 24
  _initial_editable_text_length = _tweet_text_max_length - _image_url_length - _site_url_length
