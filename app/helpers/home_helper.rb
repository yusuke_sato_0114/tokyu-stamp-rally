# coding: utf-8
module HomeHelper
  def birthyear_collection
    (1950..2010).to_a.map do |year|
      if year == 1950
        ["#{year}年以前",year]
      elsif year == 2010
        ["#{year}年以降",year]
      else
        ["#{year}年",year]
      end
    end
  end

  def gender_collection
    User.genders.keys.map { |gender| [t("activerecord.enum.user.gender.#{gender}"), gender] }
  end

  def theme_collection
    User.themes.keys.map { |theme| [t("activerecord.enum.user.theme.#{theme}"), theme] }
  end

  def job_collection
    User.jobs.keys.map { |job| [t("activerecord.enum.user.job.#{job}"), job] }
  end

  def frequency_of_use_tokyu_collection
    User.frequency_of_use_tokyus.keys.map { |fout| [t("activerecord.enum.user.frequency_of_use_tokyu.#{fout}"), fout] }
  end

  def season_top_ranking_duration
    season_start = Time.parse Settings.top_ranking.season_start.to_s
    season_end   = Time.parse Settings.top_ranking.season_end.to_s

    "#{Settings.top_ranking.season_name}(#{season_start.to_s(:season_ranking)}~#{season_end.to_s(:season_ranking)})"
  end

  def season_self_ranking_duration
    season_start = Time.parse Settings.self_ranking.season_start.to_s
    season_end   = Time.parse Settings.self_ranking.season_end.to_s

    "#{Settings.self_ranking.season_name}(#{season_start.to_s(:season_ranking)}~#{season_end.to_s(:season_ranking)})"
  end

  def weekday time
    %w(日 月 火 水 木 金 土)[time.wday]
  end
end
