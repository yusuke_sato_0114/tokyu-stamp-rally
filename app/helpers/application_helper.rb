# coding: utf-8
module ApplicationHelper
  def default_meta_tags
    {
      reverse: true,
      site: 'おそ急さんスタンプラリー2016年夏',
      title: '',
      description: '',
      keywords: 'おそ急スタンプラリー,おそ急さん,おそ急,東急スタンプラリー,東急電鉄,東急,おそスタ,おそ松さん,おそ松,スタンプラリー',
      og: {
        title: :title,
        type: 'website',
        url: "https://tokyu.rev-stamp.com",
        image: "https://tokyu.rev-stamp.com/static/application/img/ogp.jpg",
        site_name: :site,
        description: '【ファン必見！】東急沿線でおそ急さんスタンプラリー開催中！！！あなたは何急推し？？？スタンプを集めて限定グッズをゲットしよう！',
        locale: 'ja_JP'
      },
      twitter: {
        card: 'summary_large_image',
        site: '@tokyu_lines',
        title: :title,
        description: '【ファン必見！】東急沿線でおそ急さんスタンプラリー開催中！！！あなたは何急推し？？？スタンプを集めて限定グッズをゲットしよう！',
        image: {
          src: "https://tokyu.rev-stamp.com/static/application/img/ogp.jpg"
        }
      },
      charset: 'UTF-8'
    }
  end

  def hbr(str)
    str = html_escape(str)
    str.gsub(/\r\n|\r|\n/, '<br />').html_safe
  end

  def nobr(str)
    str = html_escape(str)
    str.gsub(/\r\n|\r|\n/,'').html_safe
  end

  def replace_link(text)
    text.gsub(/https?:\/\/(\w*[a-zA-ZＡ-Ｚａ-ｚ0-9０-９ぁ-ヶ亜-黑\.\/\?\#\%\&]+)/) do |t|
      text = text.sub(Regexp.new(t),"<a href=\"#{t}\" target=\"_blank\">#{t}</a>")
    end

    text.gsub(/[#＃](\w*[a-zA-ZＡ-Ｚａ-ｚ0-9０-９ぁ-ヶ亜-黑]+)/) do |t|
      text = text.sub(Regexp.new(t),"<a href=\"#{Settings.twitter.url_search_hashtag}#{t.slice(1..-1)}\" target=\"_blank\">#{t}</a>")
    end

    return text.html_safe
  end

  def shorten_modal_notification_title_label str
    str.length > 12 ? "#{str.slice 0, 11}..." : str
  end

  def shorten_modal_faq_title_label str
    str.length > 18 ? "#{str.slice 0, 17}..." : str
  end
end
