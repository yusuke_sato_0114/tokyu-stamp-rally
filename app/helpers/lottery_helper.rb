module LotteryHelper
  def mailer_href lottery_id, lottery_pass, lottery_code, win_url, user_name, user_serial_code
    subject = Settings.lottery.unique_serial_mail.subject.clone
    body = Settings.lottery.unique_serial_mail.body.clone

    body.gsub! /\{\{user_name\}\}/, user_name
    body.gsub! /\{\{user_serial_code\}\}/, user_serial_code
    body.gsub! /\{\{lottery_id\}\}/, lottery_id
    body.gsub! /\{\{lottery_pass\}\}/, lottery_pass
    body.gsub! /\{\{lottery_code\}\}/, lottery_code
    body.gsub! /\{\{win_url\}\}/, win_url

    "mailto:?subject=#{CGI.escape subject}&body=#{CGI.escape body}"
  end
end
