# coding: utf-8
class ContactMailer < ActionMailer::Base
  default from: 'sonic@revue.co.jp'
  default to: 'sonic@revue.co.jp'

  def create_new contact
    set_smtp_settings
    @contact = contact

    mail subject: 'おそ急スタンプへのお問い合わせ'
  end

  def received contact
    set_smtp_settings
    @contact = contact

    mail to: @contact.email, subject: 'おそ急スタンプへのお問い合わせを承りました'
  end

  private
  def set_smtp_settings
    ActionMailer::Base.smtp_settings = {
      address: 'smtp.gmail.com',
      port: '587',
      domain: 'smtp.gmail.com',
      authentication: "login",
      user_name: "sonic@revue.co.jp",
      password: ENV['ACTION_MAILER_GMAIL_PASSWORD']
    }
  end 
end
