#!/usr/bin/ruby

anphabe = *('a'..'z')
number = *('1'..'9')
character_set = anphabe + number
times_set = 50000
codes = times_set.times.map { character_set.sample(8).join }

while codes.uniq.count < 100000
  codes = codes.uniq
  codes << (100000 - codes.count).times.map { character_set.sample(8).join }
  codes.flatten!
end

aFile = File.new("serial_codes.csv", "w")
if aFile
   codes.uniq.count.times do |i|
     aFile.syswrite(codes[i]+ "\n")
   end
   puts "You have #{codes.uniq.count} codes"
else
   puts "Unable to open file!"
end
